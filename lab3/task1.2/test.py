import os, imp

def test():
    pets = []
    for mymodule in os.listdir('plugins'):
        moduleName, moduleExt = os.path.splitext(mymodule)
        if moduleExt == '.py':
            pet = myfactory(moduleName)('Ljubimac ' + str(len(pets)))
            pets.append(pet)
    for pet in pets:
        printGreeting(pet)
        printMenu(pet)

def myfactory(moduleName):
    file, pathname, description = imp.find_module('plugins/' + moduleName)
    module = imp.load_module(moduleName, file, pathname, description)
    return getattr(module, moduleName)

def printGreeting(pet):
    print(pet.name(), 'pozdravlja:', pet.greet())

def printMenu(pet):
    print(pet.name(), 'voli', pet.menu())

if __name__ == '__main__':
    test()
