package hr.fer.ooup.lab3.editor.actions.move;

import hr.fer.ooup.lab3.editor.TextEditorModel;

public class MoveLeftAction extends MoveAction {

    public MoveLeftAction(TextEditorModel model) {
        super(model);
    }

    @Override
    protected void moveCursor() {
        model.moveCursorLeft();
    }
}
