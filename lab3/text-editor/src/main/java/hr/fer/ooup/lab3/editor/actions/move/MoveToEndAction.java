package hr.fer.ooup.lab3.editor.actions.move;

import hr.fer.ooup.lab3.editor.TextEditorModel;
import hr.fer.ooup.lab3.editor.location.Location;

import java.util.Iterator;

public class MoveToEndAction extends MoveAction {

    public MoveToEndAction(TextEditorModel model) {
        super("Cursor to document end", model);
    }

    @Override
    protected void moveCursor() {
        model.setCursorToEnd();
    }
}
