package hr.fer.ooup.lab3.editor.actions.clipboard;

import hr.fer.ooup.lab3.editor.TextEditorModel;
import hr.fer.ooup.lab3.editor.clipboard.ClipboardObserver;
import hr.fer.ooup.lab3.editor.clipboard.ClipboardStack;
import hr.fer.ooup.lab3.editor.location.LocationRange;

import java.awt.event.ActionEvent;

public class PastePeekAction extends ClipboardAction implements ClipboardObserver {

    public PastePeekAction(TextEditorModel model, ClipboardStack clipboard) {
        super("Paste", model, clipboard);
        clipboard.registerObserver(this);
        setEnabled(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (clipboard.isEmpty()) return;
        LocationRange range = model.getSelectionRange();
        if (!range.isEmpty()) {
            model.deleteRange(range);
        }
        model.insert(clipboard.peek());
        model.resetSelectionRange();
    }

    @Override
    public void updateClipboard() {
        setEnabled(!clipboard.isEmpty());
    }
}
