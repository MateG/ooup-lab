package hr.fer.ooup.lab3.editor.actions.clipboard;

import hr.fer.ooup.lab3.editor.SelectionObserver;
import hr.fer.ooup.lab3.editor.TextEditorModel;
import hr.fer.ooup.lab3.editor.clipboard.ClipboardStack;

import java.awt.event.ActionEvent;

public class CopyAction extends ClipboardAction implements SelectionObserver {

    public CopyAction(TextEditorModel model, ClipboardStack clipboard) {
        super("Copy", model, clipboard);
        model.registerSelectionObserver(this);
        setEnabled(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String selected = ClipboardUtil.getSelected(model);
        if (selected == null) return;
        clipboard.push(selected);
    }

    @Override
    public void selectionEnabled() {
        setEnabled(true);
    }

    @Override
    public void selectionDisabled() {
        setEnabled(false);
    }
}
