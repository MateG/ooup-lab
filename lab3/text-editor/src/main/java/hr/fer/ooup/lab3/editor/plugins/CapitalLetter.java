package hr.fer.ooup.lab3.editor.plugins;

import hr.fer.ooup.lab3.editor.TextEditorModel;
import hr.fer.ooup.lab3.editor.clipboard.ClipboardStack;
import hr.fer.ooup.lab3.editor.undoable.UndoManager;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class CapitalLetter implements Plugin {
    @Override
    public String getName() {
        return "Capital letter";
    }

    @Override
    public String getDescription() {
        return "Set all starting letters to capital letters";
    }

    @Override
    public void execute(TextEditorModel model, UndoManager undoManager,
            ClipboardStack clipboard) {
        List<String> lines = model.getLines();
        for (int i = 0; i < lines.size(); i ++) {
            lines.set(i, toUpper(lines.get(i)));
        }
        model.setCursorLocation(model.getCursorLocation());
    }

    private String toUpper(String original) {
        char[] chars = original.toCharArray();
        char[] newChars = Arrays.copyOf(chars, chars.length);
        boolean upperAwaits = true;
        for (int i = 0; i < chars.length; i ++) {
            char c = chars[i];
            if (Character.isWhitespace(c)) {
                upperAwaits = true;
                continue;
            }
            if (upperAwaits) {
                newChars[i] = Character.toUpperCase(c);
                upperAwaits = false;
            }
        }
        return new String(newChars);
    }
}
