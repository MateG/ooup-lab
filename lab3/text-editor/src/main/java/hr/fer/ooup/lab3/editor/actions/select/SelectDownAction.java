package hr.fer.ooup.lab3.editor.actions.select;

import hr.fer.ooup.lab3.editor.TextEditorModel;

public class SelectDownAction extends SelectAction {

    public SelectDownAction(TextEditorModel model) {
        super(model);
    }

    @Override
    protected void moveCursor() {
        model.moveCursorDown();
    }
}
