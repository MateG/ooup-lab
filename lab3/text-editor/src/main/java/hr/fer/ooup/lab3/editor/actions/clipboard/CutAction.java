package hr.fer.ooup.lab3.editor.actions.clipboard;

import hr.fer.ooup.lab3.editor.SelectionObserver;
import hr.fer.ooup.lab3.editor.TextEditorModel;
import hr.fer.ooup.lab3.editor.clipboard.ClipboardStack;

import java.awt.event.ActionEvent;

public class CutAction extends ClipboardAction implements SelectionObserver {

    public CutAction(TextEditorModel model, ClipboardStack clipboard) {
        super("Cut", model, clipboard);
        model.registerSelectionObserver(this);
        setEnabled(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String selected = ClipboardUtil.getSelected(model);
        if (selected == null) return;
        clipboard.push(selected);
        model.deleteRange(model.getSelectionRange());
    }

    @Override
    public void selectionEnabled() {
        setEnabled(true);
    }

    @Override
    public void selectionDisabled() {
        setEnabled(false);
    }
}
