package hr.fer.ooup.lab3.editor;

public interface SelectionObserver {

    void selectionEnabled();

    void selectionDisabled();
}
