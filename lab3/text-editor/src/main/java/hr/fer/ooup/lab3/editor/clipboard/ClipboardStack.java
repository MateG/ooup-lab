package hr.fer.ooup.lab3.editor.clipboard;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class ClipboardStack {

    private Stack<String> texts = new Stack<>();

    private List<ClipboardObserver> observers = new ArrayList<>();

    public void push(String text) {
        texts.push(text);
        observers.forEach(ClipboardObserver::updateClipboard);
    }

    public String pop() {
        String popped = texts.pop();
        observers.forEach(ClipboardObserver::updateClipboard);
        return popped;
    }

    public String peek() {
        return texts.peek();
    }

    public boolean isEmpty() {
        return texts.isEmpty();
    }

    public void clear() {
        texts.clear();
        observers.forEach(ClipboardObserver::updateClipboard);
    }

    public void registerObserver(ClipboardObserver observer) {
        observers.add(observer);
    }

    public void removeObserver(ClipboardObserver observer) {
        observers.remove(observer);
    }
}
