package hr.fer.ooup.lab3.editor.actions.move;

import hr.fer.ooup.lab3.editor.TextEditorModel;
import hr.fer.ooup.lab3.editor.location.Location;

public class MoveToStartAction extends MoveAction {

    public MoveToStartAction(TextEditorModel model) {
        super("Cursor to document start", model);
    }

    @Override
    protected void moveCursor() {
        model.setCursorLocation(new Location(0, 0));
    }
}
