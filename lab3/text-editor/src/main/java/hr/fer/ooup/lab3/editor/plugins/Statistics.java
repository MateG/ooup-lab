package hr.fer.ooup.lab3.editor.plugins;

import hr.fer.ooup.lab3.editor.TextEditorModel;
import hr.fer.ooup.lab3.editor.clipboard.ClipboardStack;
import hr.fer.ooup.lab3.editor.undoable.UndoManager;

import javax.swing.*;
import java.util.Iterator;

public class Statistics implements Plugin {

    @Override
    public String getName() {
        return "Statistics";
    }

    @Override
    public String getDescription() {
        return "Show the current document statistic information";
    }

    @Override
    public void execute(TextEditorModel model, UndoManager undoManager,
            ClipboardStack clipboard) {
        Iterator<String> it = model.allLines();
        int lineCount = 0;
        int wordCount = 0;
        int charCount = 0;
        while (it.hasNext()) {
            lineCount ++;
            String line = it.next();
            wordCount += line.split("\\s+").length;
            charCount += line.length();
        }

        String message = "Line count: " + lineCount
                + "\nWord count: " + wordCount
                + "\nCharacter count: " + charCount;
        JOptionPane.showMessageDialog(null, message, "Statistics",
                JOptionPane.INFORMATION_MESSAGE);
    }
}
