package hr.fer.ooup.lab3.editor.actions.delete;

import hr.fer.ooup.lab3.editor.SelectionObserver;
import hr.fer.ooup.lab3.editor.TextEditorModel;
import hr.fer.ooup.lab3.editor.location.LocationRange;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class DeleteSelectionAction extends AbstractAction implements SelectionObserver {

    private TextEditorModel model;

    public DeleteSelectionAction(TextEditorModel model) {
        super("Delete selection");
        this.model = model;
        setEnabled(false);
        model.registerSelectionObserver(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        LocationRange range = model.getSelectionRange();
        if (range.isEmpty()) return;
        model.deleteRange(range);
    }

    @Override
    public void selectionEnabled() {
        setEnabled(true);
    }

    @Override
    public void selectionDisabled() {
        setEnabled(false);
    }
}
