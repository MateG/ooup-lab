package hr.fer.ooup.lab3.editor.plugins;

import hr.fer.ooup.lab3.editor.TextEditorModel;
import hr.fer.ooup.lab3.editor.clipboard.ClipboardStack;
import hr.fer.ooup.lab3.editor.undoable.UndoManager;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class PluginAction extends AbstractAction {

    private Plugin plugin;

    private TextEditorModel model;

    private UndoManager undoManager = UndoManager.getInstance();

    private ClipboardStack clipboard;

    public PluginAction(Plugin plugin, TextEditorModel model,
            ClipboardStack clipboard) {
        super(plugin.getName());
        this.plugin = plugin;
        this.model = model;
        this.clipboard = clipboard;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        plugin.execute(model, undoManager, clipboard);
    }
}
