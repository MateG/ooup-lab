package hr.fer.ooup.lab3.editor;

import hr.fer.ooup.lab3.editor.location.Location;
import hr.fer.ooup.lab3.editor.location.LocationRange;
import hr.fer.ooup.lab3.editor.undoable.UndoManager;
import hr.fer.ooup.lab3.editor.undoable.EditAction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class TextEditorModel {

    private List<String> lines;

    private Location cursorLocation = new Location(0, 0);

    private LocationRange selectionRange = new LocationRange(
            cursorLocation, cursorLocation);

    private List<CursorObserver> cursorObservers = new ArrayList<>();

    private List<TextObserver> textObservers = new ArrayList<>();

    private List<SelectionObserver> selectionObservers = new ArrayList<>();

    public TextEditorModel(String text) {
        this.lines = new ArrayList<>(Arrays.asList(text.split("\\n", -1)));
    }

    public void setLines(List<String> lines) {
        this.lines = lines;
        cursorLocation = new Location(0, 0);
        selectionRange = new LocationRange(cursorLocation, cursorLocation);
        fireTextChanged();
        fireCursorChanged();
    }

    public List<String> getLines() {
        return lines;
    }

    public int getLineCount() {
        return lines.size();
    }

    public Location getCursorLocation() {
        return cursorLocation;
    }

    public void setCursorLocation(Location cursorLocation) {
        this.cursorLocation = cursorLocation;
        fireCursorChanged();
    }

    public void setCursorToEnd() {
        int row = lines.size() - 1;
        setCursorLocation(new Location(row, lines.get(row).length()));
    }

    public LocationRange getSelectionRange() {
        return selectionRange;
    }

    public void setSelectionRange(LocationRange selectionRange) {
        this.selectionRange = selectionRange;
        fixSelectionRange();
        fireCursorChanged();
        if (selectionRange.isEmpty()) {
            selectionObservers.forEach(SelectionObserver::selectionDisabled);
        } else {
            selectionObservers.forEach(SelectionObserver::selectionEnabled);
        }
    }

    public void resetSelectionRange() {
        setSelectionRange(new LocationRange(cursorLocation, cursorLocation));
    }

    public Iterator<String> allLines() {
        return lines.iterator();
    }

    public Iterator<String> linesRange(int fromIndex, int toIndex) {
        return lines.subList(fromIndex, toIndex).iterator();
    }

    public void registerCursorObserver(CursorObserver observer) {
        cursorObservers.add(observer);
    }

    public void removeCursorObserver(CursorObserver observer) {
        cursorObservers.remove(observer);
    }

    public void registerTextObserver(TextObserver observer) {
        textObservers.add(observer);
    }

    public void removeTextObserver(TextObserver observer) {
        textObservers.remove(observer);
    }

    public void registerSelectionObserver(SelectionObserver observer) {
        selectionObservers.add(observer);
    }

    public void removeSelectionObserver(SelectionObserver observer) {
        selectionObservers.remove(observer);
    }

    public void moveCursorLeft() {
        Location newLocation = new Location(
                cursorLocation.getRow(), cursorLocation.getColumn()-1);
        if (outLeft(newLocation)) {
            newLocation.setRow(newLocation.getRow()-1);
            if (outUp(newLocation)) return;
            newLocation.setColumn(lines.get(newLocation.getRow()).length());
        }
        setCursorLocation(newLocation);
    }

    public void moveCursorRight() {
        Location newLocation = new Location(
                cursorLocation.getRow(), cursorLocation.getColumn()+1);
        if (outRight(newLocation)) {
            newLocation.setRow(newLocation.getRow()+1);
            if (outDown(newLocation)) return;
            newLocation.setColumn(0);
        }
        setCursorLocation(newLocation);
    }

    public void moveCursorUp() {
        Location newLocation = new Location(
                cursorLocation.getRow()-1, cursorLocation.getColumn());
        if (outUp(newLocation)) {
            if (newLocation.getColumn() == 0) return;
            newLocation.setRow(0);
            newLocation.setColumn(0);
        } else if (outRight(newLocation)) {
            newLocation.setColumn(lines.get(newLocation.getRow()).length());
        }
        setCursorLocation(newLocation);
    }

    public void moveCursorDown() {
        int oldRow = cursorLocation.getRow();
        Location newLocation = new Location(
                oldRow+1, cursorLocation.getColumn());
        if (outDown(newLocation)) {
            int oldRowLast = lines.get(oldRow).length();
            if (newLocation.getColumn() == oldRowLast) return;
            newLocation.setRow(oldRow);
            newLocation.setColumn(oldRowLast);
        } else if (outRight(newLocation)) {
            newLocation.setColumn(lines.get(newLocation.getRow()).length());
        }
        setCursorLocation(newLocation);
    }

    public void deleteBefore() {
        int row = cursorLocation.getRow();
        int col = cursorLocation.getColumn();
        int newRow = row;
        int newCol = col-1;

        if (newCol < 0) {
            newRow --;
            if (newRow < 0) return;
            int newRowCopy = newRow;
            int upperLength = lines.get(newRow).length();

            executeUndoableAction(new EditAction() {
                @Override
                public void executeDo() {
                    doDeleteBeforeAtStart(row, newRowCopy);
                }

                @Override
                public void executeUndo() {
                    doInsertNewLine(newRowCopy, upperLength);
                }
            });
        } else {
            int newRowCopy = newRow;
            char deleted = lines.get(newRow).charAt(newCol);
            executeUndoableAction(new EditAction() {
                @Override
                public void executeDo() {
                    doDeleteBefore(col, newRowCopy, newCol);
                }

                @Override
                public void executeUndo() {
                    doInsert(row, newCol, deleted);
                }
            });
        }
    }

    public void deleteAfter() {
        int col = cursorLocation.getColumn();
        int row = cursorLocation.getRow();
        String line = lines.get(row);
        if (col < line.length()) {
            char deleted = line.charAt(col);
            executeUndoableAction(new EditAction() {
                @Override
                public void executeDo() {
                    doDeleteAfter(row, col, line);
                }

                @Override
                public void executeUndo() {
                    doInsert(row, col, deleted);
                    setCursorLocation(new Location(row, col));
                }
            });
        } else {
            if (row < lines.size()-1) {
                executeUndoableAction(new EditAction() {
                    @Override
                    public void executeDo() {
                        doDeleteAfterAtEnd(row, line);
                    }

                    @Override
                    public void executeUndo() {
                        doInsertNewLine(row, col);
                    }
                });
            }
        }
    }

    public void deleteRange(LocationRange range) {
        Location start = range.getStart();
        Location end = range.getEnd();
        int rowStart = start.getRow();
        int colStart = start.getColumn();
        int rowEnd = end.getRow();
        int colEnd = end.getColumn();

        if (rowStart == rowEnd) {
            String text = lines.get(rowStart).substring(colStart, colEnd);
            executeUndoableAction(new EditAction() {
                @Override
                public void executeDo() {
                    doDeleteRangeLine(rowStart, colStart, colEnd);
                }

                @Override
                public void executeUndo() {
                    doInsertRangeLine(rowStart, colStart, text);
                }
            });
        } else {
            String[] parts = new String[rowEnd-rowStart+1];
            parts[0] = lines.get(rowStart).substring(colStart);
            for (int i = 1; i < parts.length - 1; i ++) {
                parts[i] = lines.get(rowStart + i);
            }
            parts[parts.length - 1] = lines.get(rowEnd).substring(0, colEnd);

            executeUndoableAction(new EditAction() {
                @Override
                public void executeDo() {
                    doDeleteRangeLines(rowStart, colStart, rowEnd, colEnd);
                }

                @Override
                public void executeUndo() {
                    doInsertRangeLines(rowStart, colStart, parts);
                }
            });
        }
    }

    public void insert(char c) {
        int row = cursorLocation.getRow();
        int col = cursorLocation.getColumn();
        if (c != 10) {
            executeUndoableAction(new EditAction() {
                @Override
                public void executeDo() {
                    doInsert(row, col, c);
                }

                @Override
                public void executeUndo() {
                    doDeleteBefore(col+1, row, col);
                }
            });
        } else {
            executeUndoableAction(new EditAction() {
                @Override
                public void executeDo() {
                    doInsertNewLine(row, col);
                }

                @Override
                public void executeUndo() {
                    doDeleteBeforeAtStart(row+1, row);
                }
            });
        }
    }

    public void insert(String text) {
        String[] parts = text.split("\\n", -1);
        int row = cursorLocation.getRow();
        int col = cursorLocation.getColumn();

        if (parts.length == 1) {
            executeUndoableAction(new EditAction() {
                @Override
                public void executeDo() {
                    doInsertRangeLine(row, col, text);
                }

                @Override
                public void executeUndo() {
                    doDeleteRangeLine(row, col, col + text.length());
                }
            });
        } else {
            executeUndoableAction(new EditAction() {
                @Override
                public void executeDo() {
                    doInsertRangeLines(row, col, parts);
                }

                @Override
                public void executeUndo() {
                    doDeleteRangeLines(row, col, row + parts.length - 1,
                            parts[parts.length-1].length());
                }
            });
        }
    }

    private void fireCursorChanged() {
        cursorObservers.forEach(o -> o.updateCursorLocation(cursorLocation));
    }

    private void fireTextChanged() {
        textObservers.forEach(TextObserver::updateText);
    }

    private boolean outLeft(Location location) {
        return location.getColumn() < 0;
    }

    private boolean outRight(Location location) {
        return location.getColumn() > lines.get(location.getRow()).length();
    }

    private boolean outUp(Location location) {
        return location.getRow() < 0;
    }

    private boolean outDown(Location location) {
        return location.getRow() >= lines.size();
    }

    private void fixSelectionRange() {
        Location start = selectionRange.getStart();
        Location end = selectionRange.getEnd();
        if (start.compareTo(end) > 0) {
            selectionRange.setStart(end);
            selectionRange.setEnd(start);
        }
    }

    private void executeUndoableAction(EditAction action) {
        action.executeDo();
        UndoManager.getInstance().push(action);
    }

    private void doDeleteBeforeAtStart(int row, int newRow) {
        int newCol = lines.get(newRow).length();
        String lineBelow = lines.get(row);
        String modified = lines.get(newRow).concat(lineBelow);
        lines.set(newRow, modified);
        lines.remove(row);
        setCursorLocation(new Location(newRow, newCol));
        resetSelectionRange();
        fireTextChanged();
    }

    private void doDeleteBefore(int col, int newRow, int newCol) {
        String line = lines.get(newRow);
        String modified = line.substring(0, newCol)
                .concat(line.substring(col));
        lines.set(newRow, modified);
        setCursorLocation(new Location(newRow, newCol));
        resetSelectionRange();
        fireTextChanged();
    }

    private void doDeleteAfter(int row, int col, String line) {
        lines.set(row, line.substring(0, col)
                .concat(line.substring(col+1)));
        resetSelectionRange();
        fireTextChanged();
    }

    private void doDeleteAfterAtEnd(int row, String line) {
        lines.set(row, line.concat(lines.get(row+1)));
        lines.remove(row+1);
        resetSelectionRange();
        fireTextChanged();
    }

    private void doDeleteRangeLine(int rowStart, int colStart, int colEnd) {
        String current = lines.get(rowStart);
        lines.set(rowStart, current.substring(0, colStart).concat(
                current.substring(colEnd)));
        Location newLocation = new Location(rowStart, colStart);
        if (!newLocation.equals(cursorLocation)) {
            setCursorLocation(newLocation);
        }
        resetSelectionRange();
        fireTextChanged();
    }

    private void doDeleteRangeLines(int rowStart, int colStart,
            int rowEnd, int colEnd) {
        if (rowEnd > rowStart + 1) {
            lines.subList(rowStart + 1, rowEnd).clear();
        }

        String first = lines.get(rowStart).substring(0, colStart);
        String last = lines.get(rowStart+1).substring(colEnd);
        lines.set(rowStart, first.concat(last));
        lines.remove(rowStart+1);
        Location newLocation = new Location(rowStart, colStart);
        if (!newLocation.equals(cursorLocation)) {
            setCursorLocation(newLocation);
        }
        resetSelectionRange();
        fireTextChanged();
    }

    private void doInsert(int row, int col, char c) {
        String original = lines.get(row);
        String modified = original.substring(0, col)
                + c + original.substring(col);
        lines.set(row, modified);
        setCursorLocation(new Location(row, col+1));
        resetSelectionRange();
        fireTextChanged();
    }

    private void doInsertNewLine(int row, int col) {
        String original = lines.get(row);
        lines.set(row, original.substring(0, col));
        lines.add(row+1, original.substring(col));
        setCursorLocation(new Location(row+1, 0));
        resetSelectionRange();
        fireTextChanged();
    }

    private void doInsertRangeLine(int row, int col, String text) {
        String line = lines.get(row);
        lines.set(row, line.substring(0, col) + text + line.substring(col));
        setCursorLocation(new Location(row, col + text.length()));
        resetSelectionRange();
        fireTextChanged();
    }

    private void doInsertRangeLines(int row, int col, String[] parts) {
        String first = lines.get(row);
        lines.set(row, first.substring(0, col) + parts[0]);
        int i;
        for (i = 1; i < parts.length - 1; i ++) {
            lines.add(row + i, parts[i]);
        }
        int lastRow = row + i;
        String lastPart = parts[i];
        lines.add(lastRow, lastPart.concat(first.substring(col)));
        setCursorLocation(new Location(lastRow, lastPart.length()));
        resetSelectionRange();
        fireTextChanged();
    }
}
