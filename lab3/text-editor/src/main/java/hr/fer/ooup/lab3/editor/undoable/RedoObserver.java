package hr.fer.ooup.lab3.editor.undoable;

public interface RedoObserver {

    void redoEnabled();

    void redoDisabled();
}
