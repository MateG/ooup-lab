package hr.fer.ooup.lab3.editor.actions.menu;

import hr.fer.ooup.lab3.editor.TextEditorModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

public class ClearAction extends AbstractAction {

    private TextEditorModel model;

    public ClearAction(TextEditorModel model) {
        super("Clear document");
        this.model = model;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        List<String> lines = new ArrayList<>();
        lines.add("");
        model.setLines(lines);
    }
}
