package hr.fer.ooup.lab3.editor.actions.clipboard;

import hr.fer.ooup.lab3.editor.TextEditorModel;
import hr.fer.ooup.lab3.editor.clipboard.ClipboardStack;

import javax.swing.*;

public abstract class ClipboardAction extends AbstractAction {

    protected TextEditorModel model;

    protected ClipboardStack clipboard;

    public ClipboardAction(String name, TextEditorModel model,
            ClipboardStack clipboard) {
        super(name);
        this.model = model;
        this.clipboard = clipboard;
    }
}
