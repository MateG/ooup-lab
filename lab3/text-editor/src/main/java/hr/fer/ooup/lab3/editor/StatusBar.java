package hr.fer.ooup.lab3.editor;

import hr.fer.ooup.lab3.editor.location.Location;

import javax.swing.*;
import javax.swing.border.BevelBorder;

public class StatusBar extends JLabel implements CursorObserver {

    private TextEditorModel model;

    public StatusBar(TextEditorModel model) {
        this.model = model;
        model.registerCursorObserver(this);
        setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        setHorizontalAlignment(RIGHT);
        updateText();
    }

    @Override
    public void updateCursorLocation(Location location) {
        updateText();
    }

    private void updateText() {
        Location cursor = model.getCursorLocation();
        setText("Ln: " + (cursor.getRow()+1)
                + "     Col: " + (cursor.getColumn()+1)
                + "          lines: " + model.getLineCount() + "  ");
    }
}
