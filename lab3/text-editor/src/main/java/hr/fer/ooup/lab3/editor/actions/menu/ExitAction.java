package hr.fer.ooup.lab3.editor.actions.menu;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class ExitAction extends AbstractAction {

    private JFrame frame;

    public ExitAction(JFrame frame) {
        super("Exit");
        this.frame = frame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        showExitDialog(frame);
    }

    public static void showExitDialog(JFrame frame) {
        if (JOptionPane.showConfirmDialog(frame,
                "Are you sure you want to quit? Any unsaved data will be lost.",
                "Attention",
                JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
            return;
        }

        frame.dispose();
    }
}
