package hr.fer.ooup.lab3.editor.actions.undo;

import hr.fer.ooup.lab3.editor.undoable.RedoObserver;
import hr.fer.ooup.lab3.editor.undoable.UndoManager;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class RedoAction extends AbstractAction implements RedoObserver {

    public RedoAction() {
        super("Redo");
        setEnabled(false);
        UndoManager.getInstance().registerRedoObserver(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        UndoManager.getInstance().redo();
    }

    @Override
    public void redoEnabled() {
        setEnabled(true);
    }

    @Override
    public void redoDisabled() {
        setEnabled(false);
    }
}
