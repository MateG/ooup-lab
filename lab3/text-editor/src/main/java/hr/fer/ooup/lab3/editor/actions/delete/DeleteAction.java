package hr.fer.ooup.lab3.editor.actions.delete;

import hr.fer.ooup.lab3.editor.TextEditorModel;
import hr.fer.ooup.lab3.editor.location.LocationRange;

import javax.swing.*;
import java.awt.event.ActionEvent;

public abstract class DeleteAction extends AbstractAction {

    protected TextEditorModel model;

    public DeleteAction(TextEditorModel model) {
        this.model = model;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        LocationRange range = model.getSelectionRange();
        if (range.isEmpty()) {
            deleteAtCursor();
        } else {
            model.deleteRange(range);
        }
    }

    protected abstract void deleteAtCursor();
}
