package hr.fer.ooup.lab3.editor;

import hr.fer.ooup.lab3.editor.location.Location;

public interface CursorObserver {

    void updateCursorLocation(Location location);
}
