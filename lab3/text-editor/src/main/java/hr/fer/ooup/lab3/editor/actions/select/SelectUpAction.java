package hr.fer.ooup.lab3.editor.actions.select;

import hr.fer.ooup.lab3.editor.TextEditorModel;

public class SelectUpAction extends SelectAction {

    public SelectUpAction(TextEditorModel model) {
        super(model);
    }

    @Override
    protected void moveCursor() {
        model.moveCursorUp();
    }
}
