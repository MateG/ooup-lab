package hr.fer.ooup.lab3.editor;

public interface TextObserver {

    void updateText();
}
