package hr.fer.ooup.lab3.editor.actions.move;

import hr.fer.ooup.lab3.editor.TextEditorModel;

public class MoveRightAction extends MoveAction {

    public MoveRightAction(TextEditorModel model) {
        super(model);
    }

    @Override
    protected void moveCursor() {
        model.moveCursorRight();
    }
}
