package hr.fer.ooup.lab3.editor.undoable;

public interface EditAction {

    void executeDo();

    void executeUndo();
}
