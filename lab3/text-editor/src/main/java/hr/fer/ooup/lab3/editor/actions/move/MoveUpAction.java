package hr.fer.ooup.lab3.editor.actions.move;

import hr.fer.ooup.lab3.editor.TextEditorModel;

public class MoveUpAction extends MoveAction {

    public MoveUpAction(TextEditorModel model) {
        super(model);
    }

    @Override
    protected void moveCursor() {
        model.moveCursorUp();
    }
}
