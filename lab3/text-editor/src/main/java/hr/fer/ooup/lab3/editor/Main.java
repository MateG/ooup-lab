package hr.fer.ooup.lab3.editor;

import hr.fer.ooup.lab3.editor.actions.clipboard.CopyAction;
import hr.fer.ooup.lab3.editor.actions.clipboard.CutAction;
import hr.fer.ooup.lab3.editor.actions.clipboard.PastePeekAction;
import hr.fer.ooup.lab3.editor.actions.clipboard.PastePopAction;
import hr.fer.ooup.lab3.editor.actions.delete.DeleteSelectionAction;
import hr.fer.ooup.lab3.editor.actions.menu.ClearAction;
import hr.fer.ooup.lab3.editor.actions.menu.ExitAction;
import hr.fer.ooup.lab3.editor.actions.menu.OpenAction;
import hr.fer.ooup.lab3.editor.actions.menu.SaveAction;
import hr.fer.ooup.lab3.editor.actions.move.MoveToEndAction;
import hr.fer.ooup.lab3.editor.actions.move.MoveToStartAction;
import hr.fer.ooup.lab3.editor.actions.undo.RedoAction;
import hr.fer.ooup.lab3.editor.actions.undo.UndoAction;
import hr.fer.ooup.lab3.editor.clipboard.ClipboardStack;
import hr.fer.ooup.lab3.editor.plugins.Plugin;
import hr.fer.ooup.lab3.editor.plugins.PluginAction;
import hr.fer.ooup.lab3.editor.plugins.PluginLoader;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class Main extends JFrame {

    private TextEditorModel model;

    private ClipboardStack clipboard = new ClipboardStack();

    public Main() {
        setTitle("Text Editor");
        setSize(800, 600);
        setLocation(200, 200);

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                ExitAction.showExitDialog(Main.this);
            }
        });

        initGUI();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new Main().setVisible(true));
    }

    private void initGUI() {
        Container cp = getContentPane();
        cp.setLayout(new BorderLayout());

        model = new TextEditorModel(
                "Prvi red\nDrugi\nTreći\nČetvrti\nPeti\nŠesti\nSedmi\nOsmi");
        TextEditor editor = new TextEditor(model, clipboard);
        editor.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                editor.requestFocusInWindow();
            }
        });
        cp.add(editor, BorderLayout.CENTER);

        JPanel northPanel = new JPanel(new BorderLayout());
        northPanel.add(buildMenuBar(), BorderLayout.NORTH);
        northPanel.add(buildToolBar(), BorderLayout.SOUTH);
        cp.add(northPanel, BorderLayout.NORTH);

        cp.add(new StatusBar(model), BorderLayout.SOUTH);
    }

    private JMenuBar buildMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        menuBar.add(buildFileMenu());
        menuBar.add(buildEditMenu());
        menuBar.add(buildMoveMenu());
        JMenu pluginMenu = buildPluginMenu();
        if (pluginMenu != null) menuBar.add(buildPluginMenu());
        return menuBar;
    }

    private JMenu buildFileMenu() {
        JMenu menu = new JMenu("File");
        menu.add(new JMenuItem(new OpenAction(this, model)));
        menu.add(new JMenuItem(new SaveAction(this, model)));
        menu.add(new JMenuItem(new ExitAction(this)));
        return menu;
    }

    private JMenu buildEditMenu() {
        JMenu menu = new JMenu("Edit");
        menu.add(new JMenuItem(new UndoAction()));
        menu.add(new JMenuItem(new RedoAction()));
        menu.add(new JMenuItem(new CutAction(model, clipboard)));
        menu.add(new JMenuItem(new CopyAction(model, clipboard)));
        menu.add(new JMenuItem(new PastePeekAction(model, clipboard)));
        menu.add(new JMenuItem(new PastePopAction(model, clipboard)));
        menu.add(new JMenuItem(new DeleteSelectionAction(model)));
        menu.add(new JMenuItem(new ClearAction(model)));
        return menu;
    }

    private JMenu buildMoveMenu() {
        JMenu menu = new JMenu("Move");
        menu.add(new JMenuItem(new MoveToStartAction(model)));
        menu.add(new JMenuItem(new MoveToEndAction(model)));
        return menu;
    }

    private JMenu buildPluginMenu() {
        PluginLoader loader = new PluginLoader();
        List<Plugin> plugins = loader.loadPlugins();
        if (plugins.isEmpty()) return null;

        JMenu menu = new JMenu("Plugins");
        for (Plugin plugin : plugins) {
            JMenuItem item = new JMenuItem(
                    new PluginAction(plugin, model, clipboard));
            item.setToolTipText(plugin.getDescription());
            menu.add(item);
        }
        return menu;
    }

    private JToolBar buildToolBar() {
        JToolBar toolBar = new JToolBar();
        toolBar.add(new UndoAction());
        toolBar.add(new RedoAction());
        toolBar.add(new CutAction(model, clipboard));
        toolBar.add(new CopyAction(model, clipboard));
        toolBar.add(new PastePeekAction(model, clipboard));
        return toolBar;
    }
}
