package hr.fer.ooup.lab3.editor.actions.menu;

import hr.fer.ooup.lab3.editor.TextEditorModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;

public class SaveAction extends AbstractAction {

    private JFrame frame;

    private TextEditorModel model;

    public SaveAction(JFrame frame, TextEditorModel model) {
        super("Save");
        this.frame = frame;
        this.model = model;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFileChooser chooser = new JFileChooser();
        if (chooser.showSaveDialog(frame) != JFileChooser.APPROVE_OPTION) {
            return;
        }

        Path path = chooser.getSelectedFile().toPath();
        if (Files.exists(path)) {
            if (JOptionPane.showConfirmDialog(
                    frame,
                    "Are you sure you want to overwrite "
                            + path.toString() + "?",
                    "Attention",
                    JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
                return;
            }
        }

        try (BufferedWriter writer = Files.newBufferedWriter(path)) {
            Iterator<String> it = model.allLines();
            while (true) {
                writer.write(it.next());

                if (it.hasNext()) {
                    writer.write('\n');
                } else {
                    break;
                }
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(
                    frame,
                    path.toString() + " could not be saved.",
                    "Error",
                    JOptionPane.ERROR_MESSAGE
            );
        }
    }
}
