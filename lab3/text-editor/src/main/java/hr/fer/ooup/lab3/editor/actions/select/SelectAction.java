package hr.fer.ooup.lab3.editor.actions.select;

import hr.fer.ooup.lab3.editor.TextEditorModel;
import hr.fer.ooup.lab3.editor.location.Location;
import hr.fer.ooup.lab3.editor.location.LocationRange;

import javax.swing.*;
import java.awt.event.ActionEvent;

public abstract class SelectAction extends AbstractAction {

    protected TextEditorModel model;

    public SelectAction(TextEditorModel model) {
        this.model = model;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Location cursorBefore = model.getCursorLocation();
        moveCursor();
        Location cursorAfter = model.getCursorLocation();
        LocationRange range = model.getSelectionRange();
        if (cursorBefore.equals(range.getStart())) {
            range.setStart(cursorAfter);
        } else {
            range.setEnd(cursorAfter);
        }
        model.setSelectionRange(range);
    }

    protected abstract void moveCursor();
}
