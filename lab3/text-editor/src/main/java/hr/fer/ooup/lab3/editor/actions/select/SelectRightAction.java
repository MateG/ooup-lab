package hr.fer.ooup.lab3.editor.actions.select;

import hr.fer.ooup.lab3.editor.TextEditorModel;

public class SelectRightAction extends SelectAction {

    public SelectRightAction(TextEditorModel model) {
        super(model);
    }

    @Override
    protected void moveCursor() {
        model.moveCursorRight();
    }
}
