package hr.fer.ooup.lab3.editor.actions.menu;

import hr.fer.ooup.lab3.editor.TextEditorModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class OpenAction extends AbstractAction {

    private JFrame frame;

    private TextEditorModel model;

    public OpenAction(JFrame frame, TextEditorModel model) {
        super("Open");
        this.frame = frame;
        this.model = model;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFileChooser chooser = new JFileChooser();
        if (chooser.showOpenDialog(frame) != JFileChooser.APPROVE_OPTION) {
            return;
        }

        Path path = chooser.getSelectedFile().toPath();
        if (!Files.isReadable(path)) {
            JOptionPane.showMessageDialog(
                    frame,
                    path.toString() + " does not exist.",
                    "Error",
                    JOptionPane.ERROR_MESSAGE
            );
            return;
        }

        try {
            model.setLines(Files.readAllLines(path));
        } catch (IOException e1) {
            JOptionPane.showMessageDialog(
                    frame,
                    path.toString() + " could not be opened.",
                    "Error",
                    JOptionPane.ERROR_MESSAGE
            );
        }
    }
}
