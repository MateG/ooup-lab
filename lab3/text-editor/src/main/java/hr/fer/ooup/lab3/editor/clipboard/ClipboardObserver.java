package hr.fer.ooup.lab3.editor.clipboard;

public interface ClipboardObserver {

    void updateClipboard();
}
