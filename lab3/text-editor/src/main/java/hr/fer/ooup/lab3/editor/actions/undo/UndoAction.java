package hr.fer.ooup.lab3.editor.actions.undo;

import hr.fer.ooup.lab3.editor.undoable.UndoManager;
import hr.fer.ooup.lab3.editor.undoable.UndoObserver;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class UndoAction extends AbstractAction implements UndoObserver {

    public UndoAction() {
        super("Undo");
        setEnabled(false);
        UndoManager.getInstance().registerUndoObserver(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        UndoManager.getInstance().undo();
    }

    @Override
    public void undoEnabled() {
        setEnabled(true);
    }

    @Override
    public void undoDisabled() {
        setEnabled(false);
    }
}
