package hr.fer.ooup.lab3.editor;

import hr.fer.ooup.lab3.editor.actions.clipboard.CopyAction;
import hr.fer.ooup.lab3.editor.actions.clipboard.CutAction;
import hr.fer.ooup.lab3.editor.actions.clipboard.PastePeekAction;
import hr.fer.ooup.lab3.editor.actions.clipboard.PastePopAction;
import hr.fer.ooup.lab3.editor.actions.delete.DeleteAfterAction;
import hr.fer.ooup.lab3.editor.actions.delete.DeleteBeforeAction;
import hr.fer.ooup.lab3.editor.actions.move.MoveDownAction;
import hr.fer.ooup.lab3.editor.actions.move.MoveLeftAction;
import hr.fer.ooup.lab3.editor.actions.move.MoveRightAction;
import hr.fer.ooup.lab3.editor.actions.move.MoveUpAction;
import hr.fer.ooup.lab3.editor.actions.select.SelectDownAction;
import hr.fer.ooup.lab3.editor.actions.select.SelectLeftAction;
import hr.fer.ooup.lab3.editor.actions.select.SelectRightAction;
import hr.fer.ooup.lab3.editor.actions.select.SelectUpAction;
import hr.fer.ooup.lab3.editor.actions.undo.RedoAction;
import hr.fer.ooup.lab3.editor.actions.undo.UndoAction;
import hr.fer.ooup.lab3.editor.clipboard.ClipboardStack;
import hr.fer.ooup.lab3.editor.location.Location;
import hr.fer.ooup.lab3.editor.location.LocationRange;

import javax.swing.*;
import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Iterator;
import java.util.function.BiFunction;

public class TextEditor extends JComponent implements CursorObserver, TextObserver {

    private static final Color TEXT_COLOR = Color.BLACK;
    private static final Color SELECTION_COLOR = Color.decode("#16a085");
    private static final Color CURSOR_COLOR = Color.BLACK;

    private static final int LINE_SPACING = 3;

    private static final BiFunction<Graphics2D, String, Integer> STRING_WIDTH =
            (g2d, s) -> g2d.getFontMetrics().stringWidth(s);

    private TextEditorModel model;

    private Location cursor;

    private ClipboardStack clipboard;

    public TextEditor(TextEditorModel model, ClipboardStack clipboard) {
        this.model = model;
        this.cursor = model.getCursorLocation();
        this.clipboard = clipboard;

        setupCursorObserving();
        setupTextObserving();
    }

    @Override
    public void updateCursorLocation(Location location) {
        cursor = location;
        repaint();
    }

    @Override
    public void updateText() {
        repaint();
    }

    @Override
    public boolean isFocusable() {
        return true;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;

        drawSelection(g2d);
        drawText(g2d);
        drawCursor(g2d);
    }

    private void drawSelection(Graphics2D g2d) {
        g2d.setPaint(SELECTION_COLOR);

        LocationRange range = model.getSelectionRange();
        if (range.isEmpty()) return;

        int fontHeight = g2d.getFontMetrics().getHeight();
        int offset = fontHeight + LINE_SPACING;

        Location start = range.getStart();
        Location end = range.getEnd();
        Iterator<String> it = model.linesRange(start.getRow(), end.getRow()+1);
        int y = offset*start.getRow();
        boolean first = true;
        while (it.hasNext()) {
            String line = it.next();
            int xStart = 0;
            if (first) {
                xStart = STRING_WIDTH.apply(g2d, line.substring(0, start.getColumn()));
                first = false;
            }
            int xEnd;
            if (it.hasNext()) {
                xEnd = getWidth();
            } else {
                xEnd = STRING_WIDTH.apply(g2d, line.substring(0, end.getColumn()));
            }
            g2d.fillRect(xStart, y, xEnd - xStart, offset);
            y += offset;
        }
    }

    private void drawText(Graphics2D g2d) {
        g2d.setPaint(TEXT_COLOR);

        int fontHeight = g2d.getFontMetrics().getHeight();
        int y = fontHeight;
        for (Iterator<String> it = model.allLines(); it.hasNext(); ) {
            String line = it.next();
            g2d.drawString(line, 0, y);
            y += fontHeight + LINE_SPACING;
        }
    }

    private void drawCursor(Graphics2D g2d) {
        g2d.setPaint(CURSOR_COLOR);

        int row = cursor.getRow();
        int col = cursor.getColumn();
        String line = model.linesRange(row, row+1).next().substring(0, col);
        int x = STRING_WIDTH.apply(g2d, line);
        int offset = g2d.getFontMetrics().getHeight() + LINE_SPACING;
        int yStart = cursor.getRow()*offset;
        int yEnd = yStart + offset;
        g2d.drawLine(x, yStart, x, yEnd);
    }

    private void setupCursorObserving() {
        model.registerCursorObserver(this);

        addAction(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0),
                "left", new MoveLeftAction(model));
        addAction(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0),
                "right", new MoveRightAction(model));
        addAction(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0),
                "up", new MoveUpAction(model));
        addAction(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0),
                "down", new MoveDownAction(model));
        addAction(KeyStroke.getKeyStroke(
                KeyEvent.VK_LEFT, InputEvent.SHIFT_DOWN_MASK),
                "shift left", new SelectLeftAction(model));
        addAction(KeyStroke.getKeyStroke(
                KeyEvent.VK_RIGHT, InputEvent.SHIFT_DOWN_MASK),
                "shift right", new SelectRightAction(model));
        addAction(KeyStroke.getKeyStroke(
                KeyEvent.VK_UP, InputEvent.SHIFT_DOWN_MASK),
                "shift up", new SelectUpAction(model));
        addAction(KeyStroke.getKeyStroke(
                KeyEvent.VK_DOWN, InputEvent.SHIFT_DOWN_MASK),
                "shift down", new SelectDownAction(model));
    }

    private void setupTextObserving() {
        model.registerTextObserver(this);

        addAction(KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, 0),
                "backspace", new DeleteBeforeAction(model));
        addAction(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0),
                "delete", new DeleteAfterAction(model));

        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                char c = e.getKeyChar();
                if (isPrintableChar(c)) {
                    LocationRange range = model.getSelectionRange();
                    if (!range.isEmpty()) {
                        model.deleteRange(range);
                    }
                    model.insert(c);
                }
            }
        });

        addAction(KeyStroke.getKeyStroke(KeyEvent.VK_C,
                InputEvent.CTRL_DOWN_MASK),
                "ctrl c", new CopyAction(model, clipboard));
        addAction(KeyStroke.getKeyStroke(KeyEvent.VK_X,
                InputEvent.CTRL_DOWN_MASK),
                "ctrl x", new CutAction(model, clipboard));
        addAction(KeyStroke.getKeyStroke(KeyEvent.VK_V,
                InputEvent.CTRL_DOWN_MASK),
                "ctrl v", new PastePeekAction(model, clipboard));
        addAction(KeyStroke.getKeyStroke(KeyEvent.VK_V,
                InputEvent.CTRL_DOWN_MASK | InputEvent.SHIFT_DOWN_MASK),
                "ctrl shift v", new PastePopAction(model, clipboard));

        addAction(KeyStroke.getKeyStroke(KeyEvent.VK_Z,
                InputEvent.CTRL_DOWN_MASK), "ctrl z", new UndoAction());
        addAction(KeyStroke.getKeyStroke(KeyEvent.VK_Y,
                InputEvent.CTRL_DOWN_MASK), "ctrl y", new RedoAction());
    }

    private void addAction(KeyStroke keyStroke, String key, Action action) {
        getInputMap().put(keyStroke, key);
        getActionMap().put(key, action);
    }

    private boolean isPrintableChar(char c) {
        Character.UnicodeBlock block = Character.UnicodeBlock.of(c);
        return (!Character.isISOControl(c)
                && c != KeyEvent.CHAR_UNDEFINED
                && block != null
                && block != Character.UnicodeBlock.SPECIALS
                && c != 127) || c == 10;
    }
}
