package hr.fer.ooup.lab3.editor.location;

import java.util.Comparator;
import java.util.Objects;

public class Location implements Comparable<Location> {

    private static final Comparator<Location> COMPARATOR = Comparator
            .comparingInt(Location::getRow)
            .thenComparingInt(Location::getColumn);

    private int row;

    private int column;

    public Location(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public Location copy() {
        return new Location(row, column);
    }

    @Override
    public int compareTo(Location o) {
        return COMPARATOR.compare(this, o);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return row == location.row &&
                column == location.column;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, column);
    }

    @Override
    public String toString() {
        return "(" + row + ", " + column + ')';
    }
}
