package hr.fer.ooup.lab3.editor.actions.delete;

import hr.fer.ooup.lab3.editor.TextEditorModel;

public class DeleteBeforeAction extends DeleteAction {

    public DeleteBeforeAction(TextEditorModel model) {
        super(model);
    }

    @Override
    protected void deleteAtCursor() {
        model.deleteBefore();
    }
}
