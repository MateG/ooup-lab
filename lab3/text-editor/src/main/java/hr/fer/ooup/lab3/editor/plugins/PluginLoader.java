package hr.fer.ooup.lab3.editor.plugins;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PluginLoader {

    private static final String PLUGIN_DIR = "plugins/";

    private static final String CLASS_EXT = ".class";

    @SuppressWarnings("unchecked")
    public List<Plugin> loadPlugins() {
        List<Plugin> plugins = new ArrayList<>();
        ClassLoader parent = getClass().getClassLoader();
        URLClassLoader child;
        try {
            child = new URLClassLoader(new URL[] {
                    new File(PLUGIN_DIR).toURI().toURL()
            }, parent);
        } catch (MalformedURLException ignored) {
            return plugins;
        }
        File directory = new File(PLUGIN_DIR);
        for (File file : Objects.requireNonNull(directory.listFiles())) {
            String pluginName = file.getName();
            if (!pluginName.endsWith(CLASS_EXT)) continue;
            pluginName = pluginName.substring(0,
                    pluginName.length() - CLASS_EXT.length());
            try {
                String fqdn = "hr.fer.ooup.lab3.editor.plugins." + pluginName;
                Class<Plugin> pluginClass = (Class<Plugin>) child.loadClass(fqdn);
                Plugin plugin = pluginClass.newInstance();
                plugins.add(plugin);
            } catch (ClassNotFoundException | InstantiationException
                    | IllegalAccessException ignored) {}
        }
        return plugins;
    }
}
