package hr.fer.ooup.lab3.editor.actions.clipboard;

import hr.fer.ooup.lab3.editor.TextEditorModel;
import hr.fer.ooup.lab3.editor.location.Location;
import hr.fer.ooup.lab3.editor.location.LocationRange;

import java.util.Iterator;

public final class ClipboardUtil {

    private ClipboardUtil() {
    }

    public static String getSelected(TextEditorModel model) {
        LocationRange range = model.getSelectionRange();
        if (range.isEmpty()) return null;
        Location start = range.getStart();
        Location end = range.getEnd();
        StringBuilder sb = new StringBuilder();
        Iterator<String> it = model.linesRange(
                start.getRow(), end.getRow()+1);
        boolean first = true;
        while (it.hasNext()) {
            String line = it.next();
            int iStart = 0;
            if (first) {
                iStart = start.getColumn();
                first = false;
            }
            int iEnd;
            if (it.hasNext()) {
                iEnd = line.length();
            } else {
                iEnd = end.getColumn();
            }
            sb.append(line, iStart, iEnd).append('\n');
        }
        sb.deleteCharAt(sb.length()-1);
        return sb.toString();
    }
}
