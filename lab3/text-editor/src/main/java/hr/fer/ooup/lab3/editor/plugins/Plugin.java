package hr.fer.ooup.lab3.editor.plugins;

import hr.fer.ooup.lab3.editor.TextEditorModel;
import hr.fer.ooup.lab3.editor.clipboard.ClipboardStack;
import hr.fer.ooup.lab3.editor.undoable.UndoManager;

public interface Plugin {

    String getName();

    String getDescription();

    void execute(TextEditorModel model, UndoManager undoManager,
            ClipboardStack clipboard);
}
