package hr.fer.ooup.lab3.editor.undoable;

public interface UndoObserver {

    void undoEnabled();

    void undoDisabled();
}
