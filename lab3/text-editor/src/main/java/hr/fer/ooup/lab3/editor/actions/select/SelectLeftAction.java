package hr.fer.ooup.lab3.editor.actions.select;

import hr.fer.ooup.lab3.editor.TextEditorModel;

public class SelectLeftAction extends SelectAction {

    public SelectLeftAction(TextEditorModel model) {
        super(model);
    }

    @Override
    protected void moveCursor() {
        model.moveCursorLeft();
    }
}
