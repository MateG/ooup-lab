package hr.fer.ooup.lab3.editor.undoable;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class UndoManager {

    private static UndoManager instance = new UndoManager();

    public static UndoManager getInstance() {
        return instance;
    }

    private UndoManager() {
    }

    private Stack<EditAction> undoStack = new Stack<>();

    private Stack<EditAction> redoStack = new Stack<>();

    private List<UndoObserver> undoObservers = new ArrayList<>();

    private List<RedoObserver> redoObservers = new ArrayList<>();

    public void undo() {
        if (undoStack.isEmpty()) return;
        EditAction action = popUndo();
        pushRedo(action);
        action.executeUndo();
    }

    public void redo() {
        if (redoStack.isEmpty()) return;
        EditAction action = popRedo();
        action.executeDo();
        pushUndo(action);
    }

    public void push(EditAction action) {
        clearRedo();
        pushUndo(action);
    }

    public void registerUndoObserver(UndoObserver observer) {
        undoObservers.add(observer);
    }

    public void removeUndoObserver(UndoObserver observer) {
        undoObservers.remove(observer);
    }

    public void registerRedoObserver(RedoObserver observer) {
        redoObservers.add(observer);
    }

    public void removeRedoObserver(RedoObserver observer) {
        redoObservers.remove(observer);
    }

    private void pushUndo(EditAction action) {
        undoStack.push(action);
        if (undoStack.size() == 1) {
            undoObservers.forEach(UndoObserver::undoEnabled);
        }
    }

    private EditAction popUndo() {
        if (undoStack.size() == 1) {
            undoObservers.forEach(UndoObserver::undoDisabled);
        }
        return undoStack.pop();
    }

    private void pushRedo(EditAction action) {
        redoStack.push(action);
        if (redoStack.size() == 1) {
            redoObservers.forEach(RedoObserver::redoEnabled);
        }
    }

    private EditAction popRedo() {
        if (redoStack.size() == 1) {
            redoObservers.forEach(RedoObserver::redoDisabled);
        }
        return redoStack.pop();
    }

    private void clearRedo() {
        redoStack.clear();
        redoObservers.forEach(RedoObserver::redoDisabled);
    }
}
