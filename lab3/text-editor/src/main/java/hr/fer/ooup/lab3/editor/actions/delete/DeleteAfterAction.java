package hr.fer.ooup.lab3.editor.actions.delete;

import hr.fer.ooup.lab3.editor.TextEditorModel;

public class DeleteAfterAction extends DeleteAction {

    public DeleteAfterAction(TextEditorModel model) {
        super(model);
    }

    @Override
    protected void deleteAtCursor() {
        model.deleteAfter();
    }
}
