package hr.fer.ooup.lab3.editor.actions.move;

import hr.fer.ooup.lab3.editor.TextEditorModel;
import hr.fer.ooup.lab3.editor.location.Location;
import hr.fer.ooup.lab3.editor.location.LocationRange;

import javax.swing.*;
import java.awt.event.ActionEvent;

public abstract class MoveAction extends AbstractAction {

    protected TextEditorModel model;

    public MoveAction(TextEditorModel model) {
        this.model = model;
    }

    public MoveAction(String name, TextEditorModel model) {
        super(name);
        this.model = model;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        moveCursor();
        Location cursor = model.getCursorLocation();
        model.setSelectionRange(new LocationRange(cursor, cursor));
    }

    protected abstract void moveCursor();
}
