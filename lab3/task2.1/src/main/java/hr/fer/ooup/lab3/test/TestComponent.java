package hr.fer.ooup.lab3.test;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class TestComponent extends JComponent {

    public TestComponent(JFrame frame) {
        frame.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    frame.dispose();
                }
            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;

        int width = getWidth();
        int height = getHeight();

        drawSampleLines(g2d, width, height);
        drawSampleText(g2d, width, height);
    }

    private void drawSampleLines(Graphics2D g2d, int width, int height) {
        int xStart = (int) (0.1*width);
        int xEnd = (int) (0.9*width);
        int yStart = (int) (0.1*height);
        int yEnd = (int) (0.9*height);
        g2d.drawLine(xStart, yStart, xStart, yEnd);
        g2d.drawLine(xStart, height/2, xEnd, height/2);
    }

    private void drawSampleText(Graphics2D g2d, int width, int height) {
        int x = (int) (0.3*width);
        int yFirst = (int) (0.3*height);
        int ySecond = (int) (0.7*height);
        g2d.drawString("Ovo je prva rečenica...", x, yFirst);
        g2d.drawString("...a ovo je druga", x, ySecond);
    }
}
