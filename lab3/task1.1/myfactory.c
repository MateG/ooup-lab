#include "myfactory.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

typedef void* (*PTRCREATE)();

typedef void (*PTRCONSTRUCT)();

static void* construct(void* handle, void* memory, const char* name) {
    PTRCONSTRUCT constructor;
    constructor = dlsym(handle, "construct");
    if (dlerror() != NULL) return NULL;
    constructor(memory, name);
    return memory;
}

static void* create(void* handle, const char* name) {
    PTRCREATE creator;
    creator = dlsym(handle, "create");
    if (dlerror() != NULL) return NULL;
    return creator(name);
}

void* myfactory(void* memory, const char* libname, const char* ctorarg) {
    void* handle;
    char filename[261]; // "./" + max_filename + ".so" + '\0'
    strcpy(filename, "./");
    strcat(filename, libname);
    strcat(filename, ".so");

    handle = dlopen(filename, RTLD_LAZY);
    if (handle == NULL) {
        return NULL;
    }

    return memory == NULL ?
        create(handle, ctorarg) : construct(handle, memory, ctorarg);
}
