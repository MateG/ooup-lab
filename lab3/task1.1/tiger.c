#include <malloc.h>

typedef const char* (*PTRFUN)();

struct Tiger {
    PTRFUN* vtable_;
    const char* name_;
};

const char* name(struct Tiger* this) {
    return this->name_;
}

const char* greet() {
    return "Mijau!";
}

const char* menu() {
    return "mlako mlijeko";
}

static PTRFUN Tiger_vtable[] = {
    name,
    greet,
    menu
};

void construct(struct Tiger* tiger, const char* name) {
    tiger->vtable_ = Tiger_vtable;
    tiger->name_ = name;
}

void* create(const char* name) {
    struct Tiger* tiger = (struct Tiger*) malloc(sizeof(struct Tiger));
    construct(tiger, name);
    return tiger;
}
