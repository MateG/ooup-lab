#include <stdio.h>
#include <stdlib.h>
#include "myfactory.h"

typedef const char* (*PTRFUN)();

struct Animal {
    PTRFUN* vtable_;
    // vtable entries:
    // 0: const char* name(void* this);
    // 1: const char* greet();
    // 2: const char* menu();
};

// parrots and tigers defined in respective dynamic libraries

void animalPrintGreeting(struct Animal* animal) {
    printf("%s pozdravlja: %s\n",
        animal->vtable_[0](animal), animal->vtable_[1]());
}

void animalPrintMenu(struct Animal* animal) {
    printf("%s voli %s\n",
        animal->vtable_[0](animal), animal->vtable_[2]());
}

void testHeap() {
    printf("Testing heap...\n");

    struct Animal* p1 = (struct Animal*)
        myfactory(NULL, "parrot", "Modrobradi");
    struct Animal* p2 = (struct Animal*)
        myfactory(NULL, "tiger", "Straško");
    if (!p1 || !p2) {
        printf("Creation of plug-in objects failed.\n");
        exit(1);
    }

    animalPrintGreeting(p1); // "Sto mu gromova!"
    animalPrintGreeting(p2); // "Mijau!"

    animalPrintMenu(p1); // "brazilske orahe"
    animalPrintMenu(p2); // "mlako mlijeko"

    free(p1);
    free(p2);
}

struct NamedAnimal {
    PTRFUN* vtable_;
    const char* name_;
};

void testStack() {
    printf("Testing stack...\n");

    struct NamedAnimal a1;
    if (myfactory(&a1, "parrot", "Modrobradi") == NULL) {
        printf("Creation of plug-in objects failed.\n");
        exit(1);
    }
    struct NamedAnimal a2;
    if (myfactory(&a2, "tiger", "Straško") == NULL) {
        printf("Creation of plug-in objects failed.\n");
        exit(1);
    }

    animalPrintGreeting((struct Animal*) &a1); // "Sto mu gromova!"
    animalPrintGreeting((struct Animal*) &a2); // "Mijau!"

    animalPrintMenu((struct Animal*) &a1); // "brazilske orahe"
    animalPrintMenu((struct Animal*) &a2); // "mlako mlijeko"
}

int main(void) {
    testHeap();
    testStack();
}
