#include <malloc.h>

typedef const char* (*PTRFUN)();

struct Parrot {
    PTRFUN* vtable_;
    const char* name_;
};

const char* name(struct Parrot* this) {
    return this->name_;
}

const char* greet() {
    return "Sto mu gromova!";
}

const char* menu() {
    return "brazilske orahe";
}

static PTRFUN Parrot_vtable[] = {
    name,
    greet,
    menu
};

void construct(struct Parrot* parrot, const char* name) {
    parrot->vtable_ = Parrot_vtable;
    parrot->name_ = name;
}

void* create(const char* name) {
    struct Parrot* parrot = (struct Parrot*) malloc(sizeof(struct Parrot));
    construct(parrot, name);
    return parrot;
}
