#ifndef MYFACTORY_H
#define MYFACTORY_H

void* myfactory(void* memory, const char* libname, const char* ctorarg);

#endif
