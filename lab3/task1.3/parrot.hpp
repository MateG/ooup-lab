#ifndef PARROT_H
#define PARROT_H

#include <string>
#include "animal.hpp"

class Parrot : public Animal {
private:
    const std::string& name_;
public:
    Parrot(const std::string& name);
    virtual ~Parrot();

    virtual const char* name();
    virtual const char* greet();
    virtual const char* menu();

    static void* myCreator(const std::string& arg);
    static int hreg;
};

#endif
