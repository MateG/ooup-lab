#include "myfactory.hpp"

myfactory::myfactory() {
}

myfactory::~myfactory() {
}

myfactory& myfactory::instance() {
    static myfactory theFactory;
    return theFactory;
}

int myfactory::registerCreator(const std::string& id, pFunCreator pfn) {
    this->creators_[id] = pfn;
    return 0;
}

void* myfactory::create(const std::string& id, const std::string& arg) {
    return this->creators_[id](arg);
}

std::vector<std::string> myfactory::getIds() {
    MyMap& m = this->creators_;
    std::vector<std::string> ids;
    for (MyMap::iterator it = m.begin(); it != m.end(); it ++) {
        ids.push_back(it->first);
    }
    return ids;
}
