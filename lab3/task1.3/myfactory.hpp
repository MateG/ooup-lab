#ifndef MYFACTORY_HPP
#define MYFACTORY_HPP

#include <map>
#include <vector>
#include <string>

class myfactory {
public:
    typedef void* (*pFunCreator)(const std::string&);
    typedef std::map<std::string, pFunCreator> MyMap;

    static myfactory& instance();

    int registerCreator(const std::string& id, pFunCreator pfn);
    void* create(const std::string& id, const std::string& arg);
    std::vector<std::string> getIds();
private:
    myfactory();
    ~myfactory();
    myfactory(const myfactory&);
    MyMap creators_;
};

#endif
