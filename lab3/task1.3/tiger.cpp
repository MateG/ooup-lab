#include "tiger.hpp"

#include "myfactory.hpp"

int Tiger::hreg = myfactory::instance()
    .registerCreator("tiger", myCreator);

void* Tiger::myCreator(const std::string& arg) {
    return new Tiger(arg);
}

Tiger::Tiger(const std::string& name) : name_(name) {
}

Tiger::~Tiger() {
}

const char* Tiger::name() {
    return this->name_.c_str();
}

const char* Tiger::greet() {
    return "Mijau!";
}

const char* Tiger::menu() {
    return "mlako mlijeko";
}
