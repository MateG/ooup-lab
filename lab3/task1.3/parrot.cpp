#include "parrot.hpp"

#include "myfactory.hpp"

int Parrot::hreg = myfactory::instance()
    .registerCreator("parrot", myCreator);

void* Parrot::myCreator(const std::string& arg) {
    return new Parrot(arg);
}

Parrot::Parrot(const std::string& name) : name_(name) {
}

Parrot::~Parrot() {
}

const char* Parrot::name() {
    return this->name_.c_str();
}

const char* Parrot::greet() {
    return "Sto mu gromova!";
}

const char* Parrot::menu() {
    return "brazilske orahe";
}
