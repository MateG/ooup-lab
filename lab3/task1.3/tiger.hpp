#ifndef TIGER_H
#define TIGER_H

#include <string>
#include "animal.hpp"

class Tiger : public Animal {
private:
    const std::string& name_;
public:
    Tiger(const std::string& name);
    virtual ~Tiger();

    virtual const char* name();
    virtual const char* greet();
    virtual const char* menu();

    static void* myCreator(const std::string& arg);
    static int hreg;
};

#endif
