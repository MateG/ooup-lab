package hr.fer.ooup.lab4.paint.renderer;

import hr.fer.ooup.lab4.paint.geometry.Point;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class SVGRendererImpl implements Renderer {

    private List<String> lines = new ArrayList<>();

    private String filename;

    public SVGRendererImpl(String filename) {
        this.filename = filename;
        lines.add("<svg xmlns=\"http://www.w3.org/2000/svg\"" +
                " xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
    }

    public void close() throws IOException {
        lines.add("</svg>");
        Files.write(Paths.get(filename), lines);
    }

    @Override
    public void drawLine(Point s, Point e) {
        lines.add("<line x1=\"" + s.getX() + "\"  y1=\"" + s.getY()
                + "\" x2=\"" + e.getX() + "\" y2=\"" + e.getY()
                + "\" style=\"stroke:#0000ff;\"/>");
    }

    @Override
    public void fillPolygon(Point[] points) {
        StringBuilder sb = new StringBuilder("<polygon points=\"");
        for (Point point : points) {
            sb.append(point.getX()).append(',')
                    .append(point.getY()).append(' ');
        }
        sb.deleteCharAt(sb.length()-1);
        sb.append("\" style=\"stroke:#ff0000; fill:#0000ff;\"/>");
        lines.add(sb.toString());
    }
}
