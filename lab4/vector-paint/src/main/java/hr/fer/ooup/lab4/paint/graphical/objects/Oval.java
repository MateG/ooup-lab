package hr.fer.ooup.lab4.paint.graphical.objects;

import hr.fer.ooup.lab4.paint.geometry.Point;
import hr.fer.ooup.lab4.paint.geometry.Rectangle;
import hr.fer.ooup.lab4.paint.graphical.GraphicalObject;
import hr.fer.ooup.lab4.paint.renderer.Renderer;

import java.util.List;
import java.util.Stack;

public class Oval extends AbstractGraphicalObject {

    public Oval(Point right, Point down) {
        super(new Point[] {right, down});
    }

    public Oval() {
        this(new Point(10, 0), new Point(0, 10));
    }

    @Override
    public Rectangle getBoundingBox() {
        Point right = getHotPoint(0);
        Point down = getHotPoint(1);
        int halfWidth = right.getX() - down.getX();
        int halfHeight = down.getY() - right.getY();
        return new Rectangle(
                down.getX() - halfWidth,
                right.getY() - halfHeight,
                2 * halfWidth,
                2 * halfHeight
        );
    }

    @Override
    public double selectionDistance(Point mousePoint) {
        Point right = getHotPoint(0);
        Point down = getHotPoint(1);
        int a = right.getX() - down.getX();
        int b = down.getY() - right.getY();
        int p = mousePoint.getX() - down.getX();
        int q = mousePoint.getY() - right.getY();

        if (p == 0) {
            if (q >= -b && q <= b) return 0.0;
            return Math.abs(q) - b;
        }

        if (p >= -a && p <= a && q >= -b && q <= b) {
            double yCalculated = Math.sqrt(a*a - p*p) * b/a;
            if (q <= yCalculated && q >= -yCalculated) return 0.0;
        }

        double x = a*b*p;
        x /= Math.sqrt((double) p*p * b*b + q*q * a*a);
        double y = x * q/p;
        double toPos = Math.pow(x - p, 2) + Math.pow(y - q, 2);
        double toNeg = Math.pow(x + p, 2) + Math.pow(y + q, 2);

        return Math.sqrt(Math.min(toNeg, toPos));
    }

    @Override
    public String getShapeName() {
        return "Oval";
    }

    @Override
    public GraphicalObject duplicate() {
        return new Oval(getHotPoint(0), getHotPoint(1));
    }

    @Override
    public void render(Renderer r) {
        Point right = getHotPoint(0);
        Point down = getHotPoint(1);
        int halfWidth = right.getX() - down.getX();
        int halfWidth2 = halfWidth * halfWidth;
        int halfHeight = down.getY() - right.getY();
        int halfHeight2 = halfHeight * halfHeight;
        int xCenter = down.getX();
        int yCenter = right.getY();

        Point[] points;
        int index = 0;
        if (halfWidth <= halfHeight) {
            if (halfWidth <= 0) halfWidth = 1;
            points = new Point[4 * halfWidth];
            for (int x = 0; x < halfWidth; x ++) {
                int offset = (int) (Math.sqrt(
                        halfHeight2 - (double) halfHeight2 * x*x / halfWidth2));
                points[index++] = new Point(xCenter + x, yCenter + offset);
            }
        } else {
            if (halfHeight <= 0) halfHeight = 1;
            points = new Point[4 * halfHeight];
            for (int y = -halfHeight; y < 0; y ++) {
                int offset = (int) (Math.sqrt(
                        halfWidth2 - (double) halfWidth2 * y*y / halfHeight2));
                points[index++] = new Point(xCenter + offset, yCenter + y);
            }
        }

        for (int i = index; i > 0; i --) {
            Point original = points[i-1];
            points[index++] = new Point(
                    original.getX(),
                    2*yCenter - original.getY()
            );
        }

        for (int i = index; i > 0; i --) {
            Point original = points[i-1];
            points[index++] = new Point(
                    2*xCenter - original.getX(),
                    original.getY()
            );
        }

        r.fillPolygon(points);
    }

    @Override
    public String getShapeID() {
        return "@OVAL";
    }

    @Override
    public void save(List<String> rows) {
        Point right = getHotPoint(0);
        Point down = getHotPoint(1);
        rows.add(getShapeID() + " " + right.getX() + " " + right.getY()
                + " " + down.getX() + " " + down.getY());
    }

    @Override
    public void load(Stack<GraphicalObject> stack, String data) {
        String[] parts = data.split("\\s+");
        if (parts.length != 4) {
            throw new RuntimeException("Expected 4 values for oval.");
        }

        try {
            stack.push(new Oval(
                    new Point(Integer.parseInt(parts[0]),
                            Integer.parseInt(parts[1])),
                    new Point(Integer.parseInt(parts[2]),
                            Integer.parseInt(parts[3]))));
        } catch (NumberFormatException ex) {
            throw new RuntimeException("Invalid oval values.");
        }
    }
}
