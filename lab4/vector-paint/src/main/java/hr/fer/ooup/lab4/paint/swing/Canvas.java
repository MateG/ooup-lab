package hr.fer.ooup.lab4.paint.swing;

import hr.fer.ooup.lab4.paint.geometry.Point;
import hr.fer.ooup.lab4.paint.graphical.GraphicalObject;
import hr.fer.ooup.lab4.paint.model.DocumentModel;
import hr.fer.ooup.lab4.paint.model.DocumentModelListener;
import hr.fer.ooup.lab4.paint.renderer.G2DRendererImpl;
import hr.fer.ooup.lab4.paint.renderer.Renderer;
import hr.fer.ooup.lab4.paint.state.IdleState;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import javax.swing.event.MouseInputListener;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class Canvas extends JComponent implements DocumentModelListener {

    private GUI gui;

    private DocumentModel model;

    public Canvas(GUI gui, DocumentModel model) {
        this.gui = gui;
        this.model = model;

        model.addDocumentModelListener(this);
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    gui.updateState(new IdleState());
                } else {
                    gui.getCurrentState().keyPressed(e.getKeyCode());
                }
            }
        });

        MouseInputListener mouseListener = new MouseInputAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                gui.getCurrentState().mouseDown(
                        new Point(e.getPoint().x, e.getPoint().y),
                        e.isShiftDown(), e.isControlDown());
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                gui.getCurrentState().mouseUp(
                        new Point(e.getPoint().x, e.getPoint().y),
                        e.isShiftDown(), e.isControlDown());
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                gui.getCurrentState().mouseDragged(
                        new Point(e.getPoint().x, e.getPoint().y));
            }
        };
        addMouseListener(mouseListener);
        addMouseMotionListener(mouseListener);
    }

    @Override
    public void documentChange() {
        repaint();
    }

    @Override
    public boolean isFocusable() {
        return true;
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        Renderer renderer = new G2DRendererImpl(g2d);
        for (GraphicalObject object : model.list()) {
            object.render(renderer);
            gui.getCurrentState().afterDraw(renderer, object);
        }
        gui.getCurrentState().afterDraw(renderer);
    }
}
