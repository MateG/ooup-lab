package hr.fer.ooup.lab4.paint.graphical.objects;

import hr.fer.ooup.lab4.paint.geometry.GeometryUtil;
import hr.fer.ooup.lab4.paint.geometry.Point;
import hr.fer.ooup.lab4.paint.geometry.Rectangle;
import hr.fer.ooup.lab4.paint.graphical.GraphicalObject;
import hr.fer.ooup.lab4.paint.renderer.Renderer;

import java.util.List;
import java.util.Stack;

public class LineSegment extends AbstractGraphicalObject {

    public LineSegment(Point start, Point end) {
        super(new Point[] {start, end});
    }

    public LineSegment() {
        this(new Point(0, 0), new Point(10, 0));
    }

    @Override
    public Rectangle getBoundingBox() {
        Point start = getHotPoint(0);
        Point end = getHotPoint(1);
        return new Rectangle(
                Math.min(start.getX(), end.getX()),
                Math.min(start.getY(), end.getY()),
                Math.abs(end.getX() - start.getX()),
                Math.abs(end.getY() - start.getY())
        );
    }

    @Override
    public double selectionDistance(Point mousePoint) {
        return GeometryUtil.distanceFromLineSegment(
                getHotPoint(0), getHotPoint(1), mousePoint);
    }

    @Override
    public String getShapeName() {
        return "Linija";
    }

    @Override
    public GraphicalObject duplicate() {
        return new LineSegment(getHotPoint(0), getHotPoint(1));
    }

    @Override
    public void render(Renderer r) {
        r.drawLine(getHotPoint(0), getHotPoint(1));
    }

    @Override
    public String getShapeID() {
        return "@LINE";
    }

    @Override
    public void save(List<String> rows) {
        Point start = getHotPoint(0);
        Point end = getHotPoint(1);
        rows.add(getShapeID() + " " + start.getX() + " " + start.getY()
                + " " + end.getX() + " " + end.getY());
    }

    @Override
    public void load(Stack<GraphicalObject> stack, String data) {
        String[] parts = data.split("\\s+");
        if (parts.length != 4) {
            throw new RuntimeException("Expected 4 values for line segment.");
        }

        try {
            stack.push(new LineSegment(
                    new Point(Integer.parseInt(parts[0]),
                            Integer.parseInt(parts[1])),
                    new Point(Integer.parseInt(parts[2]),
                            Integer.parseInt(parts[3]))));
        } catch (NumberFormatException ex) {
            throw new RuntimeException("Invalid line segment values.");
        }
    }
}
