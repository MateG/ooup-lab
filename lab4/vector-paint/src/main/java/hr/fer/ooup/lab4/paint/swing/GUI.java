package hr.fer.ooup.lab4.paint.swing;

import hr.fer.ooup.lab4.paint.graphical.GraphicalObject;
import hr.fer.ooup.lab4.paint.graphical.objects.CompositeShape;
import hr.fer.ooup.lab4.paint.model.DocumentModel;
import hr.fer.ooup.lab4.paint.renderer.SVGRendererImpl;
import hr.fer.ooup.lab4.paint.state.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.*;

public class GUI extends JFrame {

    private State currentState = new IdleState();

    public GUI(List<GraphicalObject> objects) {
        setTitle("Vector Paint");
        setSize(800, 600);
        setLocation(200, 200);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        initGUI(objects);
    }

    public State getCurrentState() {
        return currentState;
    }

    public void updateState(State newState) {
        currentState.onLeaving();
        currentState = newState;
    }

    private void initGUI(List<GraphicalObject> objects) {
        Container cp = getContentPane();
        cp.setLayout(new BorderLayout());

        DocumentModel model = new DocumentModel();
        Canvas canvas = new Canvas(this, model);
        cp.add(canvas, BorderLayout.CENTER);
        cp.add(buildToolBar(objects, model, canvas), BorderLayout.NORTH);
    }

    private JToolBar buildToolBar(List<GraphicalObject> objects,
            DocumentModel model, Canvas canvas) {
        JToolBar toolBar = new JToolBar();

        addButton("Učitaj", e -> openDialog(model, objects), toolBar);
        addButton("Pohrani", e -> saveDialog(model), toolBar);
        addButton("SVG Export", e -> svgExportDialog(model), toolBar);

        for (GraphicalObject object : objects) {
            addButton(object.getShapeName(),
                    e -> updateState(new AddShapeState(object, model)),
                    toolBar);
        }

        addButton("Selektiraj",
                e -> updateState(new SelectShapeState(model)), toolBar);
        addButton("Brisalo",
                e -> updateState(new EraseState(model, canvas)), toolBar);

        return toolBar;
    }

    private void addButton(String text, ActionListener listener,
            JToolBar toolBar) {
        JButton button = new JButton(text);
        button.addActionListener(listener);
        button.setFocusable(false);
        toolBar.add(button);
    }

    private void svgExportDialog(DocumentModel model) {
        JFileChooser fileChooser = new JFileChooser();
        if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            String filename = fileChooser.getSelectedFile().getAbsolutePath();
            SVGRendererImpl r = new SVGRendererImpl(filename);
            for (GraphicalObject object : model.list()) {
                object.render(r);
            }

            try {
                r.close();
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(
                        this,
                        "Could not save " + filename,
                        "Error!",
                        JOptionPane.ERROR_MESSAGE
                );
            }
        }
    }

    private void saveDialog(DocumentModel model) {
        JFileChooser fileChooser = new JFileChooser();
        if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            Path path = fileChooser.getSelectedFile().toPath();
            List<String> rows = new ArrayList<>();
            for (GraphicalObject object : model.list()) {
                object.save(rows);
            }

            try {
                Files.write(path, rows);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(
                        this,
                        "Could not save " + path,
                        "Error!",
                        JOptionPane.ERROR_MESSAGE
                );
            }
        }
    }

    private void openDialog(DocumentModel model, List<GraphicalObject> objects) {
        JFileChooser fileChooser = new JFileChooser();
        if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            Path path = fileChooser.getSelectedFile().toPath();
            try {
                List<String> lines = Files.readAllLines(path);
                Map<String, GraphicalObject> prototypes = new HashMap<>();
                objects.forEach(o -> prototypes.put(o.getShapeID(), o));
                CompositeShape composite = new CompositeShape(null);
                prototypes.put(composite.getShapeID(), composite);

                Stack<GraphicalObject> stack = new Stack<>();
                for (String line : lines) {
                    line = line.trim();
                    if (line.isEmpty()) continue;
                    String[] parts = line.split("\\s+", 2);
                    prototypes.get(parts[0]).load(stack, parts[1]);
                }

                model.clear();
                stack.forEach(model::addGraphicalObject);
            } catch (IOException e) {
                JOptionPane.showMessageDialog(
                        this,
                        "Could not open " + path,
                        "Error!",
                        JOptionPane.ERROR_MESSAGE
                );
            } catch (RuntimeException ex) {
                JOptionPane.showMessageDialog(
                        this,
                        ex.getMessage(),
                        "Error!",
                        JOptionPane.ERROR_MESSAGE
                );
            }
        }
    }
}
