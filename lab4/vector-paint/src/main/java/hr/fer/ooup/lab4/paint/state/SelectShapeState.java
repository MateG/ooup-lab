package hr.fer.ooup.lab4.paint.state;

import hr.fer.ooup.lab4.paint.geometry.Point;
import hr.fer.ooup.lab4.paint.geometry.Rectangle;
import hr.fer.ooup.lab4.paint.graphical.GraphicalObject;
import hr.fer.ooup.lab4.paint.graphical.objects.CompositeShape;
import hr.fer.ooup.lab4.paint.model.DocumentModel;
import hr.fer.ooup.lab4.paint.renderer.Renderer;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

public class SelectShapeState implements State {

    private static final int HP_SIZE = 3;

    private DocumentModel model;

    public SelectShapeState(DocumentModel model) {
        this.model = model;
    }

    @Override
    public void mouseDown(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
        GraphicalObject object = model.findSelectedGraphicalObject(mousePoint);
        if (ctrlDown) {
            if (object == null) return;
            object.setSelected(true);
        } else {
            List<GraphicalObject> selectedList = model.getSelectedObjects();
            if (selectedList.size() == 1 && selectedList.get(0) == object) {
                int index = model.findSelectedHotPoint(object, mousePoint);
                if (index == -1) return;
                object.setHotPointSelected(index, true);
            } else {
                model.list().forEach(o -> o.setSelected(false));
                if (object == null) return;
                object.setSelected(true);
            }
        }
    }

    @Override
    public void mouseUp(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
        List<GraphicalObject> selectedList = model.getSelectedObjects();
        if (selectedList.size() == 1) {
            GraphicalObject object = selectedList.get(0);
            for (int i = 0; i < object.getNumberOfHotPoints(); i ++) {
                object.setHotPointSelected(i, false);
            }
        }
    }

    @Override
    public void mouseDragged(Point mousePoint) {
        List<GraphicalObject> selectedList = model.getSelectedObjects();
        if (selectedList.size() == 1) {
            GraphicalObject object = selectedList.get(0);
            for (int i = 0; i < object.getNumberOfHotPoints(); i ++) {
                if (object.isHotPointSelected(i)) {
                    object.setHotPoint(i, mousePoint);
                    break;
                }
            }
        }
    }

    @Override
    public void keyPressed(int keyCode) {
        List<GraphicalObject> selectedList = model.getSelectedObjects();

        if (selectedList.size() == 1) {
            GraphicalObject selected = selectedList.get(0);
            switch (keyCode) {
                case KeyEvent.VK_UP:
                    selected.translate(new Point(0, -1));
                    break;
                case KeyEvent.VK_DOWN:
                    selected.translate(new Point(0, 1));
                    break;
                case KeyEvent.VK_LEFT:
                    selected.translate(new Point(-1, 0));
                    break;
                case KeyEvent.VK_RIGHT:
                    selected.translate(new Point(1, 0));
                    break;
                case KeyEvent.VK_PLUS:
                case KeyEvent.VK_ADD:
                    model.increaseZ(selected);
                    break;
                case KeyEvent.VK_MINUS:
                case KeyEvent.VK_SUBTRACT:
                    model.decreaseZ(selected);
                    break;
                case KeyEvent.VK_U:
                    if (!(selected instanceof CompositeShape)) break;
                    ungroupSelected((CompositeShape) selected);
                    break;
            }
        } else if (selectedList.size() > 1) {
            if (keyCode == KeyEvent.VK_G) {
                groupSelected(selectedList);
            }
        }
    }

    @Override
    public void afterDraw(Renderer r, GraphicalObject go) {
        if (go.isSelected()) {
            Rectangle rectangle = go.getBoundingBox();
            int x = rectangle.getX();
            int y = rectangle.getY();
            int width = rectangle.getWidth();
            int height = rectangle.getHeight();
            drawRectangle(r, new Point(x, y), new Point(x + width, y),
                    new Point(x + width, y + height), new Point(x, y + height));

            if (model.getSelectedObjects().size() == 1) {
                for (int i = 0; i < go.getNumberOfHotPoints(); i ++) {
                    Point hotPoint = go.getHotPoint(i);
                    int xh = hotPoint.getX();
                    int yh = hotPoint.getY();
                    drawRectangle(r, new Point(xh - HP_SIZE, yh - HP_SIZE),
                            new Point(xh + HP_SIZE, yh - HP_SIZE),
                            new Point(xh + HP_SIZE, yh + HP_SIZE),
                            new Point(xh - HP_SIZE, yh + HP_SIZE));
                }
            }
        }
    }

    @Override
    public void afterDraw(Renderer r) {
    }

    @Override
    public void onLeaving() {
        model.list().forEach(o -> {
            o.setSelected(false);
            resetHotPointSelection(o);
        });
    }

    private void drawRectangle(Renderer r, Point a, Point b, Point c, Point d) {
        r.drawLine(a, b);
        r.drawLine(b, c);
        r.drawLine(c, d);
        r.drawLine(d, a);
    }

    private void resetHotPointSelection(GraphicalObject object) {
        for (int i = 0; i < object.getNumberOfHotPoints(); i ++) {
            object.setHotPointSelected(i, false);
        }
    }

    private void groupSelected(List<GraphicalObject> selectedList) {
        List<GraphicalObject> groupList = new ArrayList<>(selectedList);
        groupList.forEach(o -> model.removeGraphicalObject(o));
        CompositeShape group = new CompositeShape(groupList);
        model.addGraphicalObject(group);
        group.setSelected(true);
    }

    private void ungroupSelected(CompositeShape composite) {
        List<GraphicalObject> children = composite.getChildren();
        composite.setSelected(false);
        model.removeGraphicalObject(composite);
        for (GraphicalObject child : children) {
            model.addGraphicalObject(child);
            child.setSelected(true);
        }
    }
}
