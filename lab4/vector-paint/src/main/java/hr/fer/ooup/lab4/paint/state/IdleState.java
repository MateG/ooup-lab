package hr.fer.ooup.lab4.paint.state;

import hr.fer.ooup.lab4.paint.geometry.Point;
import hr.fer.ooup.lab4.paint.graphical.GraphicalObject;
import hr.fer.ooup.lab4.paint.renderer.Renderer;

public class IdleState implements State {

    @Override
    public void mouseDown(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
    }

    @Override
    public void mouseUp(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
    }

    @Override
    public void mouseDragged(Point mousePoint) {
    }

    @Override
    public void keyPressed(int keyCode) {
    }

    @Override
    public void afterDraw(Renderer r, GraphicalObject go) {
    }

    @Override
    public void afterDraw(Renderer r) {
    }

    @Override
    public void onLeaving() {
    }
}
