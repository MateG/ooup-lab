package hr.fer.ooup.lab4.paint.state;

import hr.fer.ooup.lab4.paint.geometry.Point;
import hr.fer.ooup.lab4.paint.graphical.GraphicalObject;
import hr.fer.ooup.lab4.paint.renderer.Renderer;

public interface State {

    void mouseDown(Point mousePoint, boolean shiftDown, boolean ctrlDown);

    void mouseUp(Point mousePoint, boolean shiftDown, boolean ctrlDown);

    void mouseDragged(Point mousePoint);

    void keyPressed(int keyCode);

    void afterDraw(Renderer r, GraphicalObject go);

    void afterDraw(Renderer r);

    void onLeaving();
}
