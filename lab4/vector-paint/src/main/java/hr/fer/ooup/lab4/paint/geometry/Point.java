package hr.fer.ooup.lab4.paint.geometry;

public class Point {

    final int x;

    final int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Point translate(Point other) {
        return new Point(this.x + other.x, this.y + other.y);
    }

    public Point difference(Point other) {
        return new Point(this.x - other.x, this.y - other.y);
    }
}
