package hr.fer.ooup.lab4.paint.geometry;

public final class GeometryUtil {

    private GeometryUtil() {
    }

    public static double distanceFromPoint(Point p1, Point p2) {
        return Math.sqrt(distanceSquared(p1, p2));
    }

    public static double distanceFromLineSegment(Point s, Point e, Point p) {
        double lineLength2 = distanceSquared(s, e);
        if (lineLength2 == 0.0) return distanceFromPoint(p, s);
        double t = ((p.x - s.x) * (e.x - s.x) + (p.y - s.y) * (e.y - s.y))
                / lineLength2;
        t = Math.max(0, Math.min(1, t));
        return Math.sqrt(distanceSquared(
                p.x, p.y, s.x + t * (e.x - s.x), s.y + t * (e.y - s.y)
        ));
    }

    private static double distanceSquared(Point p1, Point p2) {
        return distanceSquared(p1.x, p1.y, p2.x, p2.y);
    }

    private static double distanceSquared(double p1x, double p1y,
            double p2x, double p2y) {
        double deltaX = p1x - p2x;
        double deltaY = p1y - p2y;
        return deltaX*deltaX + deltaY*deltaY;
    }
}
