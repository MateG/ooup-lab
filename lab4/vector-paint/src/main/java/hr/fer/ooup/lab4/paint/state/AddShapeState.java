package hr.fer.ooup.lab4.paint.state;

import hr.fer.ooup.lab4.paint.geometry.Point;
import hr.fer.ooup.lab4.paint.graphical.GraphicalObject;
import hr.fer.ooup.lab4.paint.model.DocumentModel;
import hr.fer.ooup.lab4.paint.renderer.Renderer;

public class AddShapeState implements State {

    private GraphicalObject prototype;

    private DocumentModel model;

    public AddShapeState(GraphicalObject prototype, DocumentModel model) {
        this.prototype = prototype;
        this.model = model;
    }

    @Override
    public void mouseDown(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
        GraphicalObject object = prototype.duplicate();
        object.translate(mousePoint);
        model.addGraphicalObject(object);
    }

    @Override
    public void mouseUp(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
    }

    @Override
    public void mouseDragged(Point mousePoint) {
    }

    @Override
    public void keyPressed(int keyCode) {
    }

    @Override
    public void afterDraw(Renderer r, GraphicalObject go) {
    }

    @Override
    public void afterDraw(Renderer r) {
    }

    @Override
    public void onLeaving() {
    }
}
