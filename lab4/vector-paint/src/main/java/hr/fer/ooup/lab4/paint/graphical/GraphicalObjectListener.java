package hr.fer.ooup.lab4.paint.graphical;

public interface GraphicalObjectListener {

    void graphicalObjectChanged(GraphicalObject go);

    void graphicalObjectSelectionChanged(GraphicalObject go);
}
