package hr.fer.ooup.lab4.paint.renderer;

import hr.fer.ooup.lab4.paint.geometry.Point;

public interface Renderer {

    void drawLine(Point s, Point e);

    void fillPolygon(Point[] points);
}
