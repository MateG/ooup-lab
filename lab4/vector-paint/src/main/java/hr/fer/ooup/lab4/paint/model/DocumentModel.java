package hr.fer.ooup.lab4.paint.model;

import hr.fer.ooup.lab4.paint.geometry.Point;
import hr.fer.ooup.lab4.paint.graphical.GraphicalObject;
import hr.fer.ooup.lab4.paint.graphical.GraphicalObjectListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DocumentModel implements GraphicalObjectListener {

    public final static double SELECTION_PROXIMITY = 10;

    private List<GraphicalObject> objects = new ArrayList<>();

    private List<GraphicalObject> roObjects = Collections.unmodifiableList(objects);

    private List<DocumentModelListener> listeners = new ArrayList<>();

    private List<GraphicalObject> selectedObjects = new ArrayList<>();

    private List<GraphicalObject> roSelectedObjects = Collections.unmodifiableList(selectedObjects);

    public void clear() {
        objects.forEach(o -> o.removeGraphicalObjectListener(this));
        objects.clear();
        notifyListeners();
    }

    public void addGraphicalObject(GraphicalObject go) {
        objects.add(go);
        go.addGraphicalObjectListener(this);
        notifyListeners();
    }

    public void removeGraphicalObject(GraphicalObject go) {
        objects.remove(go);
        go.setSelected(false);
        go.removeGraphicalObjectListener(this);
        notifyListeners();
    }

    public List<GraphicalObject> list() {
        return roObjects;
    }

    public void addDocumentModelListener(DocumentModelListener l) {
        listeners.add(l);
    }

    public void removeDocumentModelListener(DocumentModelListener l) {
        listeners.remove(l);
    }

    public void notifyListeners() {
        listeners.forEach(DocumentModelListener::documentChange);
    }

    public List<GraphicalObject> getSelectedObjects() {
        return roSelectedObjects;
    }

    public void increaseZ(GraphicalObject go) {
        int index = objects.indexOf(go);
        if (index != objects.size() - 1) {
            objects.remove(index);
            objects.add(index + 1, go);
            notifyListeners();
        }
    }

    public void decreaseZ(GraphicalObject go) {
        int index = objects.lastIndexOf(go);
        if (index != 0) {
            objects.remove(index);
            objects.add(index - 1, go);
            notifyListeners();
        }
    }

    public GraphicalObject findSelectedGraphicalObject(Point mousePoint) {
        double closestDistance = Double.MAX_VALUE;
        GraphicalObject closest = null;
        for (GraphicalObject object : objects) {
            double distance = object.selectionDistance(mousePoint);
            if (distance <= SELECTION_PROXIMITY) {
                if (distance <= closestDistance) {
                    closestDistance = distance;
                    closest = object;
                }
            }
        }
        return closest;
    }

    public int findSelectedHotPoint(GraphicalObject object, Point mousePoint) {
        double closestDistance = Double.MAX_VALUE;
        int selectedIndex = -1;
        for (int i = 0; i < object.getNumberOfHotPoints(); i ++) {
            double distance = object.getHotPointDistance(i, mousePoint);
            if (distance <= SELECTION_PROXIMITY) {
                if (distance < closestDistance) {
                    closestDistance = distance;
                    selectedIndex = i;
                }
            }
        }
        return selectedIndex;
    }

    @Override
    public void graphicalObjectChanged(GraphicalObject go) {
        notifyListeners();
    }

    @Override
    public void graphicalObjectSelectionChanged(GraphicalObject go) {
        if (go.isSelected() && !selectedObjects.contains(go)) {
            selectedObjects.add(go);
        } else {
            selectedObjects.remove(go);
        }
        notifyListeners();
    }
}
