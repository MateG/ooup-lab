package hr.fer.ooup.lab4.paint.model;

public interface DocumentModelListener {

    void documentChange();
}
