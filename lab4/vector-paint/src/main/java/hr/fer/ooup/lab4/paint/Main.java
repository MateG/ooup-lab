package hr.fer.ooup.lab4.paint;

import hr.fer.ooup.lab4.paint.graphical.GraphicalObject;
import hr.fer.ooup.lab4.paint.graphical.objects.LineSegment;
import hr.fer.ooup.lab4.paint.graphical.objects.Oval;
import hr.fer.ooup.lab4.paint.swing.GUI;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            List<GraphicalObject> objects = new ArrayList<>();
            objects.add(new LineSegment());
            objects.add(new Oval());
            GUI gui = new GUI(objects);
            gui.setVisible(true);
        });
    }
}
