package hr.fer.ooup.lab4.paint.renderer;

import hr.fer.ooup.lab4.paint.geometry.Point;

import java.awt.*;

public class G2DRendererImpl implements Renderer {

    private Graphics2D g2d;

    public G2DRendererImpl(Graphics2D g2d) {
        this.g2d = g2d;
    }

    @Override
    public void drawLine(Point s, Point e) {
        g2d.setPaint(Color.BLUE);
        g2d.drawLine(s.getX(), s.getY(), e.getX(), e.getY());
    }

    @Override
    public void fillPolygon(Point[] points) {
        g2d.setPaint(Color.BLUE);
        int nPoints = points.length;
        int[] xPoints = new int[nPoints];
        for (int i = 0; i < xPoints.length; i ++) {
            xPoints[i] = points[i].getX();
        }
        int[] yPoints = new int[nPoints];
        for (int i = 0; i < yPoints.length; i ++) {
            yPoints[i] = points[i].getY();
        }
        g2d.fillPolygon(xPoints, yPoints, nPoints);

        g2d.setPaint(Color.RED);
        g2d.drawPolyline(xPoints, yPoints, nPoints);
    }
}
