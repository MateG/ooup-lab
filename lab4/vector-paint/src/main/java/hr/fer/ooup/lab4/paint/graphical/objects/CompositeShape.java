package hr.fer.ooup.lab4.paint.graphical.objects;

import hr.fer.ooup.lab4.paint.geometry.Point;
import hr.fer.ooup.lab4.paint.geometry.Rectangle;
import hr.fer.ooup.lab4.paint.graphical.GraphicalObject;
import hr.fer.ooup.lab4.paint.renderer.Renderer;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class CompositeShape extends AbstractGraphicalObject {

    private List<GraphicalObject> children;

    public CompositeShape(List<GraphicalObject> children) {
        super(new Point[0]);
        this.children = children;
    }

    public List<GraphicalObject> getChildren() {
        return children;
    }

    @Override
    public Rectangle getBoundingBox() {
        Rectangle first = children.get(0).getBoundingBox();
        int xMin = first.getX();
        int yMin = first.getY();
        int xMax = xMin + first.getWidth();
        int yMax = yMin + first.getHeight();
        for (int i = 1; i < children.size(); i ++) {
            Rectangle r = children.get(i).getBoundingBox();
            int rx = r.getX();
            int ry = r.getY();
            xMin = Math.min(xMin, rx);
            xMax = Math.max(xMax, rx + r.getWidth());
            yMin = Math.min(yMin, ry);
            yMax = Math.max(yMax, ry + r.getHeight());
        }
        return new Rectangle(xMin, yMin, xMax - xMin, yMax - yMin);
    }

    @Override
    public double selectionDistance(Point mousePoint) {
        return children.stream()
                .mapToDouble(c -> c.selectionDistance(mousePoint))
                .min().orElse(Double.MAX_VALUE);
    }

    @Override
    public void render(Renderer r) {
        children.forEach(o -> o.render(r));
    }

    @Override
    public String getShapeName() {
        return "Kompozit";
    }

    @Override
    public GraphicalObject duplicate() {
        return null;
    }

    @Override
    public void translate(Point delta) {
        for (GraphicalObject child : children) {
            child.translate(delta);
        }
        notifyListeners();
    }

    @Override
    public String getShapeID() {
        return "@COMP";
    }

    @Override
    public void save(List<String> rows) {
        for (GraphicalObject child : children) {
            child.save(rows);
        }
        rows.add(getShapeID() + " " + children.size());
    }

    @Override
    public void load(Stack<GraphicalObject> stack, String data) {
        try {
            int count = Integer.parseInt(data);
            List<GraphicalObject> children = new ArrayList<>();
            for (int i = 0; i < count; i ++) {
                children.add(0, stack.pop());
            }
            stack.push(new CompositeShape(children));
        } catch (NumberFormatException ex) {
            throw new RuntimeException("Invalid composite shape count value.");
        }
    }
}
