package hr.fer.ooup.lab4.paint.state;

import hr.fer.ooup.lab4.paint.geometry.Point;
import hr.fer.ooup.lab4.paint.graphical.GraphicalObject;
import hr.fer.ooup.lab4.paint.model.DocumentModel;
import hr.fer.ooup.lab4.paint.renderer.Renderer;
import hr.fer.ooup.lab4.paint.swing.Canvas;

import java.util.ArrayList;
import java.util.List;

public class EraseState implements State {

    private DocumentModel model;

    private Canvas canvas;

    private List<Point> points = new ArrayList<>();

    public EraseState(DocumentModel model, Canvas canvas) {
        this.model = model;
        this.canvas = canvas;
    }

    @Override
    public void mouseDown(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
    }

    @Override
    public void mouseUp(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
        for (Point point : points) {
            GraphicalObject object = model.findSelectedGraphicalObject(point);
            if (object == null) continue;
            model.removeGraphicalObject(object);
        }
        points.clear();
        canvas.repaint();
    }

    @Override
    public void mouseDragged(Point mousePoint) {
        points.add(mousePoint);
        canvas.repaint();
    }

    @Override
    public void keyPressed(int keyCode) {
    }

    @Override
    public void afterDraw(Renderer r, GraphicalObject go) {
    }

    @Override
    public void afterDraw(Renderer r) {
        int size = points.size();
        if (size < 2) return;

        Point previous = points.get(0);
        for (int i = 1; i < size; i ++) {
            Point current = points.get(i);
            r.drawLine(previous, current);
            previous = current;
        }
    }

    @Override
    public void onLeaving() {
    }
}
