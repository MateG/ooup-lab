#include <iostream>

class B {
public:
    virtual int prva() = 0;
    virtual int druga() = 0;
};

class D : public B {
public:
    virtual int prva() { return 0; }
    virtual int druga() { return 42; }
};

typedef int (*PTRFUN)();

void printValues(B* pb) {
    PTRFUN* func_table = *(PTRFUN**) pb;
    std::cout << func_table[0]() << '\n'
        << func_table[1]() << std::endl;
}

int main(void) {
    D d;
    printValues(&d);
    return 0;
}
