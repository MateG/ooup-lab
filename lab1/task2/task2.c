#include <stdio.h>
#include <malloc.h>
#include "task2.h"

PTRFUN func_table_UnaryFunction[] = {
    NULL, UnaryFunction_negative_value_at
};

void constructUnaryFunction(UnaryFunction* function,
        int lower_bound, int upper_bound) {
    function->func_table_ = func_table_UnaryFunction;
    function->lower_bound_ = lower_bound;
    function->upper_bound_ = upper_bound;
}

UnaryFunction* createUnaryFunction(int lower_bound, int upper_bound) {
    UnaryFunction* function = (UnaryFunction*) malloc(sizeof(UnaryFunction));
    constructUnaryFunction(function, lower_bound, upper_bound);
    return function;
}

double UnaryFunction_negative_value_at(UnaryFunction* function, double x) {
    return -v_value_at(function, x);
}

void UnaryFunction_tabulate(UnaryFunction* function) {
    int x;
    for (x = function->lower_bound_; x <= function->upper_bound_; x ++) {
        printf("f(%d)=%f\n", x, v_value_at(function, (double) x));
    }
}

bool UnaryFunction_same_functions_for_ints(UnaryFunction* f1,
        UnaryFunction* f2, double tolerance) {
    int x;
    if (f1->lower_bound_ != f2->lower_bound_) return false;
    if (f1->upper_bound_ != f2->upper_bound_) return false;
    for (x = f1->lower_bound_; x <= f1->upper_bound_; x ++) {
        double delta = v_value_at(f1, x) - v_value_at(f2, x);
        if (delta < 0) delta = -delta;
        if (delta > tolerance) return false;
    }
    return true;
}

PTRFUN func_table_Square[] = {
    Square_value_at, UnaryFunction_negative_value_at
};

void constructSquare(Square* square, int lower_bound, int upper_bound) {
    square->func_table_ = func_table_Square;
    square->lower_bound_ = lower_bound;
    square->upper_bound_ = upper_bound;
}

Square* createSquare(int lower_bound, int upper_bound) {
    Square* square = (Square*) malloc(sizeof(Square));
    constructSquare(square, lower_bound, upper_bound);
    return square;
}

double Square_value_at(Square* square, double x) {
    return x*x;
}

PTRFUN func_table_Linear[] = {
    Linear_value_at, UnaryFunction_negative_value_at
};

void constructLinear(Linear* linear, int lower_bound, int upper_bound,
        double a_coef, double b_coef) {
    linear->func_table_ = func_table_Linear;
    linear->lower_bound_ = lower_bound;
    linear->upper_bound_ = upper_bound;
    linear->a_ = a_coef;
    linear->b_ = b_coef;
}

Linear* createLinear(int lower_bound, int upper_bound,
        double a_coef, double b_coef) {
    Linear* linear = (Linear*) malloc(sizeof(Linear));
    constructLinear(linear, lower_bound, upper_bound, a_coef, b_coef);
    return linear;
}

double Linear_value_at(Linear* linear, double x) {
    return linear->a_*x + linear->b_;
}

int main(void) {
    UnaryFunction* f1 = (UnaryFunction*) createSquare(-2, 2);
    UnaryFunction_tabulate(f1);
    UnaryFunction* f2 = (UnaryFunction*) createLinear(-2, 2, 5, -2);
    UnaryFunction_tabulate(f2);
    printf("f1==f2: %s\n",
            UnaryFunction_same_functions_for_ints(f1, f2, 1E-6) ?
            "DA" : "NE");
    printf("neg_val f2(1) = %lf\n", v_negative_value_at(f2, 1.0));
    free(f1);
    free(f2);
    return 0;
}
