#ifndef TASK2_H
#define TASK2_H

typedef enum {
    false, true
} bool;

typedef double (*PTRFUN)();

typedef struct {
    PTRFUN* func_table_;
    int lower_bound_;
    int upper_bound_;
} UnaryFunction;

#define v_value_at(function, x) \
    (((UnaryFunction*) function)->func_table_[0]((function), (x)))

#define v_negative_value_at(function, x) \
    (((UnaryFunction*) function)->func_table_[1]((function), (x)))

double UnaryFunction_negative_value_at(UnaryFunction* function, double x);

typedef struct {
    PTRFUN* func_table_;
    int lower_bound_;
    int upper_bound_;
} Square;

double Square_value_at(Square* square, double x);

typedef struct {
    PTRFUN* func_table_;
    int lower_bound_;
    int upper_bound_;
    double a_;
    double b_;
} Linear;

double Linear_value_at(Linear* linear, double x);

#endif
