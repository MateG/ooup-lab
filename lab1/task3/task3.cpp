#include <iostream>

class CoolClass
{
public:
    virtual void set(int x) { x_ = x; };
    virtual int get() { return x_; };
private:
    int x_;
};

class PlainOldClass
{
public:
    void set(int x) { x_ = x; };
    int get() { return x_; };
private:
    int x_;
};

int main(void)
{
    std::cout << "sizeof CoolClass: " << sizeof(CoolClass) << std::endl;
    std::cout << "sizeof PlainOldClass: " << sizeof(PlainOldClass) << std::endl;
    return 0;
}
