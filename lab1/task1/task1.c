#include <stdio.h>
#include <malloc.h>
#include "task1.h"

void animalPrintGreeting(struct Animal* animal) {
    printf("%s pozdravlja: %s\n", animal->name_, animal->func_table_[0]());
}

void animalPrintMenu(struct Animal* animal) {
    printf("%s voli %s\n", animal->name_, animal->func_table_[1]());
}

PTRFUN func_table_Dog[] = {
    dogGreet, dogMenu
};

void constructDog(struct Animal* dog, const char* name) {
    dog->name_ = name;
    dog->func_table_ = func_table_Dog;
}

struct Animal* createDog(const char* name) {
    struct Animal* dog = (struct Animal*) malloc(sizeof(struct Animal));
    constructDog(dog, name);
    return dog;
}

const char* dogGreet(void) {
    return "vau!";
}

const char* dogMenu(void) {
    return "kuhanu govedinu";
}

PTRFUN func_table_Cat[] = {
    catGreet, catMenu
};

void constructCat(struct Animal* cat, const char* name) {
    cat->name_ = name;
    cat->func_table_ = func_table_Cat;
}

struct Animal* createCat(const char* name) {
    struct Animal* cat = (struct Animal*) malloc(sizeof(struct Animal));
    constructCat(cat, name);
    return cat;
}

const char* catGreet(void) {
    return "mijau!";
}

const char* catMenu(void) {
    return "konzerviranu tunjevinu";
}

void testAnimals(void) {
    printf("\tTesting heap:\n");

    struct Animal* p1 = createDog("Hamlet");
    struct Animal* p2 = createCat("Ofelija");
    struct Animal* p3 = createDog("Polonije");

    animalPrintGreeting(p1);
    animalPrintGreeting(p2);
    animalPrintGreeting(p3);

    animalPrintMenu(p1);
    animalPrintMenu(p2);
    animalPrintMenu(p3);

    free(p1); free(p2); free(p3);
}

void testStack() {
    printf("\tTesting stack:\n");

    struct Animal dog;
    constructDog(&dog, "Bobi");
    struct Animal cat;
    constructCat(&cat, "Macka");

    animalPrintGreeting(&dog);
    animalPrintGreeting(&cat);

    animalPrintMenu(&dog);
    animalPrintMenu(&cat);
}

struct Animal** createDogs(int n) {
    struct Animal** dogs = (struct Animal**) malloc(n * sizeof(struct Animal*));
    for (int i = 0; i < n; i ++) {
        dogs[i] = createDog("Pas");
    }
    return dogs;
}

void testDogs() {
    printf("\tTesting dog collection:\n");

    struct Animal** dogs = createDogs(6);
    for (int i = 0; i < 6; i ++) {
        animalPrintGreeting(dogs[i]);
    }
}

int main(void) {
    testAnimals();
    testStack();
    testDogs();
    return 0;
}
