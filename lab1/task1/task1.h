#ifndef TASK1_H
#define TASK1_H

typedef const char* (*PTRFUN)();

struct Animal {
    const char* name_;
    PTRFUN* func_table_;
};

const char* dogGreet(void);
const char* dogMenu(void);

const char* catGreet(void);
const char* catMenu(void);

#endif
