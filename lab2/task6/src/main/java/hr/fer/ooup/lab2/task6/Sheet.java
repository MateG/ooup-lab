package hr.fer.ooup.lab2.task6;

import java.util.ArrayList;
import java.util.List;

public class Sheet {

    private Cell[][] cells;

    public Sheet(int rows, int columns) {
        this.cells = new Cell[rows][columns];
        for (int row = 0; row < rows; row ++) {
            for (int column = 0; column < columns; column ++) {
                this.cells[row][column] = new Cell(this);
            }
        }
    }

    public void set(String reference, String content) {
        Cell cell = cell(reference);
        getReferences(cell).forEach(r -> r.detach(cell));
        cell.setExpression(content);
        try {
            cell.setValue(Integer.parseInt(content));
        } catch (NumberFormatException ex) {
            getReferences(cell).forEach(r -> r.attach(cell));
            checkCircularDependency(cell, cell);
            cell.setValue(evaluate(cell));
        }
    }

    public void print() {
        System.out.println(this);
    }

    public Cell cell(String reference) {
        int row = parseRow(reference.charAt(0));
        int column = parseColumn(reference.substring(1));
        return cells[row][column];
    }

    public List<Cell> getReferences(Cell cell) {
        List<Cell> references = new ArrayList<>();
        String expression = cell.getExpression();
        if (expression == null || isInteger(expression)) return references;

        String[] parts = expression.split("\\+");
        for (String part : parts) {
            if (!isInteger(expression)) {
                references.add(cell(part));
            }
        }
        return references;
    }

    public int evaluate(Cell cell) {
        if (isInteger(cell.getExpression())) {
            return cell.getValue();
        }
        int value = 0;
        for (Cell reference : getReferences(cell)) {
            value += evaluate(reference);
        }
        return value;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Cell[] row : cells) {
            for (Cell cell : row) {
                sb.append(cell.getValue()).append(' ');
            }
            sb.deleteCharAt(sb.length()-1);
            sb.append('\n');
        }
        return sb.toString();
    }

    private void checkCircularDependency(Cell cell, Cell start) {
        for (Cell reference : getReferences(start)) {
            if (reference == cell) {
                throw new SheetException("Circular dependency detected");
            }
            checkCircularDependency(cell, reference);
        }
    }

    private int parseRow(char rowLabel) {
        int row;
        if (rowLabel >= 'a' && rowLabel <= 'z') {
            row = rowLabel - 'a';
        } else if (rowLabel >= 'A' && rowLabel <= 'Z') {
            row = rowLabel - 'A';
        } else {
            throw new SheetException(rowLabel + " is not a valid row");
        }
        return row;
    }

    private int parseColumn(String columnLabel) {
        int column;
        try {
            column = Integer.parseInt(columnLabel) - 1; // 1-indexed
        } catch (NumberFormatException ex) {
            throw new SheetException(columnLabel + " is not a valid column");
        }
        return column;
    }

    private boolean isInteger(String text) {
        try {
            Integer.parseInt(text);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }
}
