package hr.fer.ooup.lab2.task6;

import java.util.ArrayList;
import java.util.List;

public class Cell {

    private Sheet sheet;

    private String expression;

    private int value;

    private List<Cell> observers = new ArrayList<>();

    public Cell(Sheet sheet) {
        this.sheet = sheet;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
        observers.forEach(o -> o.setValue(sheet.evaluate(o)));
    }

    public void attach(Cell observer) {
        observers.add(observer);
    }

    public void detach(Cell observer) {
        observers.remove(observer);
    }
}
