package hr.fer.ooup.lab2.task6;

public class SheetException extends RuntimeException {

    public SheetException() {
    }

    public SheetException(String message) {
        super(message);
    }

    public SheetException(String message, Throwable cause) {
        super(message, cause);
    }

    public SheetException(Throwable cause) {
        super(cause);
    }
}
