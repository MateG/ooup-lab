#include <iostream>
#include <list>

struct Point {
    int x;
    int y;
};

class Shape {
public:
    virtual void draw() = 0;
    virtual void move(int delta_x, int delta_y) = 0;
};

class Circle : public Shape {
private:
    double radius_;
    Point center_;
public:
    virtual void draw() {
        std::cerr << "in Circle::draw\n";
    }

    virtual void move(int delta_x, int delta_y) {
        center_.x += delta_x;
        center_.y += delta_y;
        std::cerr << "in Circle::move\n";
    }
};

class Square : public Shape {
private:
    double side_;
    Point center_;
public:
    virtual void draw() {
        std::cerr << "in Square::draw\n";
    }

    virtual void move(int delta_x, int delta_y) {
        center_.x += delta_x;
        center_.y += delta_y;
        std::cerr << "in Square::move\n";
    }
};

class Rhomb : public Shape {
private:
    double side_;
    Point center_;
    double angle_;
public:
    virtual void draw() {
        std::cerr << "in Rhomb::draw\n";
    }

    virtual void move(int delta_x, int delta_y) {
        center_.x += delta_x;
        center_.y += delta_y;
        std::cerr << "in Rhomb::move\n";
    }
};

void drawShapes(const std::list<Shape*>& fig) {
    std::list<Shape*>::const_iterator it;
    for (it = fig.begin(); it != fig.end(); ++it) {
        (*it)->draw();
    }
}


void moveShapes(const std::list<Shape*>& fig, int delta_x, int delta_y) {
    std::list<Shape*>::const_iterator it;
    for (it = fig.begin(); it != fig.end(); ++it) {
        (*it)->move(delta_x, delta_y);
    }
}

int main() {
    std::list<Shape*> shapes = {
        new Circle,
        new Square,
        new Square,
        new Circle,
        new Rhomb
    };

    drawShapes(shapes);
    moveShapes(shapes, 2.0, 5.0);
}
