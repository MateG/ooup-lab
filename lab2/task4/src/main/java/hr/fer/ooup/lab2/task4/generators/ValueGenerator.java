package hr.fer.ooup.lab2.task4.generators;

public interface ValueGenerator {

    int[] getValues();
}
