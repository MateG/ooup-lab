package hr.fer.ooup.lab2.task4.percentiles;

public class NearestRankMethod implements PercentileMethod {

    @Override
    public int getPercentile(int[] values, int percent) {
        int index = (int) (Math.ceil(percent*values.length/100.0) - 1);
        return values[index];
    }
}
