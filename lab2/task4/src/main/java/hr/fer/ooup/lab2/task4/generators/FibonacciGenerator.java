package hr.fer.ooup.lab2.task4.generators;

public class FibonacciGenerator implements ValueGenerator {

    private int size;

    public FibonacciGenerator(int size) {
        if (size < 1) {
            throw new IllegalArgumentException("Size must be at least 1");
        }
        this.size = size;
    }

    @Override
    public int[] getValues() {
        int[] values = new int[size];
        values[0] = 0;
        if (size > 1) values[1] = 1;
        for (int i = 2; i < size; i ++) {
            values[i] = values[i-2] + values[i-1];
        }
        return values;
    }
}
