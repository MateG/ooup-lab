package hr.fer.ooup.lab2.task4.generators;

public class OrderedGenerator implements ValueGenerator {

    private int lowerBound;

    private int upperBound;

    private int step;

    public OrderedGenerator(int lowerBound, int upperBound, int step) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.step = step;
    }

    @Override
    public int[] getValues() {
        int size = (upperBound - lowerBound) / step + 1;
        int[] values = new int[size];
        int current = lowerBound;
        for (int i = 0; i < size; i ++) {
            values[i] = current;
            current += step;
        }
        return values;
    }
}
