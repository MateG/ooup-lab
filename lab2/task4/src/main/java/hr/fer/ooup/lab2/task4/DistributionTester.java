package hr.fer.ooup.lab2.task4;

import hr.fer.ooup.lab2.task4.generators.ValueGenerator;
import hr.fer.ooup.lab2.task4.generators.RandomGenerator;
import hr.fer.ooup.lab2.task4.percentiles.InterpolationMethod;
import hr.fer.ooup.lab2.task4.percentiles.PercentileMethod;

import java.io.PrintStream;

public class DistributionTester {

    private ValueGenerator generator;

    private PercentileMethod method;

    public DistributionTester(ValueGenerator generator,
            PercentileMethod method) {
        this.generator = generator;
        this.method = method;
    }

    public void printPercentiles(PrintStream stream) {
        int[] values = generator.getValues();
        for (int i = 10; i <= 90; i += 10) {
            int percentile = method.getPercentile(values, i);
            stream.println(i + "-th percentile = " + percentile);
        }
    }

    public static void main(String[] args) {
        new DistributionTester(
                new RandomGenerator(5, 10, 100),
                new InterpolationMethod()
        ).printPercentiles(System.out);
    }
}
