package hr.fer.ooup.lab2.task4.generators;

import java.util.Arrays;
import java.util.Random;

public class RandomGenerator implements ValueGenerator {

    private Random random = new Random();

    private double mean;

    private double sigma;

    private int size;

    public RandomGenerator(double mean, double sigma, int size) {
        this.mean = mean;
        this.sigma = sigma;
        this.size = size;
    }

    @Override
    public int[] getValues() {
        int[] values = new int[size];
        for (int i = 0; i < size; i ++) {
            values[i] = (int) Math.round(random.nextGaussian()*sigma + mean);
        }
        Arrays.sort(values);
        return values;
    }
}
