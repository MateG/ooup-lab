package hr.fer.ooup.lab2.task4.percentiles;

public class InterpolationMethod implements PercentileMethod {

    @Override
    public int getPercentile(int[] values, int percent) {
        int N = values.length;
        if (percent < 100 * (1-0.5) / N) return values[0];
        if (percent > 100 * (N-0.5) / N) return values[N-1];

        double p = 0;
        for (int i = 0; i < N; i ++) {
            double lower = 100 * (i+0.5) / N;
            double upper = 100 * (i+1.5) / N;
            if (percent >= lower && percent <= upper) {
                p = values[i] + N*(percent-lower)*(values[i+1]-values[i])/100;
                break;
            }
        }
        return (int) p;
    }
}
