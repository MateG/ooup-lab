package hr.fer.ooup.lab2.task4.percentiles;

public interface PercentileMethod {

    int getPercentile(int[] values, int percent);
}
