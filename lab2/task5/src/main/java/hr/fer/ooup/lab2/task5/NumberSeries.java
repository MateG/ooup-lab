package hr.fer.ooup.lab2.task5;

import hr.fer.ooup.lab2.task5.observers.NumberObserver;
import hr.fer.ooup.lab2.task5.sources.NumberSource;

import java.util.ArrayList;
import java.util.List;

public class NumberSeries {

    private static final long SLEEP_MILLIS = 1000;

    private List<Integer> values = new ArrayList<>();

    private NumberSource source;

    private List<NumberObserver> observers = new ArrayList<>();

    public NumberSeries(NumberSource source) {
        this.source = source;
    }

    public List<Integer> getValues() {
        return values;
    }

    public void attach(NumberObserver observer) {
        observers.add(observer);
    }

    public void detach(NumberObserver observer) {
        observers.remove(observer);
    }

    public void start() {
        while (true) {
            addValue(source.getValue());
            try {
                Thread.sleep(SLEEP_MILLIS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void addValue(int value) {
        values.add(value);
        notifyObservers();
    }

    private void notifyObservers() {
        for (NumberObserver observer : observers) {
            observer.update();
        }
    }
}
