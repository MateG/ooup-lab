package hr.fer.ooup.lab2.task5.sources;

public interface NumberSource {

    int getValue();
}
