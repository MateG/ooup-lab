package hr.fer.ooup.lab2.task5.sources;

import java.io.BufferedReader;
import java.io.IOException;

public abstract class AbstractReaderSource implements NumberSource {

    protected BufferedReader reader;

    @Override
    public int getValue() {
        int value;
        while (true) {
            try {
                String line = reader.readLine();
                try {
                    value = Integer.parseInt(line);
                    if (value >= 0) break;
                } catch (NumberFormatException ignorable) {}
            } catch (IOException e) {
                try {
                    reader.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                return -1;
            }
        }
        return value;
    }
}
