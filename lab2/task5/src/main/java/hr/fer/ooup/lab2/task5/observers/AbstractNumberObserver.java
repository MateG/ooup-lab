package hr.fer.ooup.lab2.task5.observers;

import hr.fer.ooup.lab2.task5.NumberSeries;

public abstract class AbstractNumberObserver implements NumberObserver {

    protected NumberSeries series;

    public AbstractNumberObserver(NumberSeries series) {
        this.series = series;
        series.attach(this);
    }
}
