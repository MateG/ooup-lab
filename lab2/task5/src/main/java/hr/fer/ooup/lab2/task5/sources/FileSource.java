package hr.fer.ooup.lab2.task5.sources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileSource extends AbstractReaderSource {

    public FileSource(Path path) throws IOException {
        reader = new BufferedReader(new InputStreamReader(
                Files.newInputStream(path)));
    }
}
