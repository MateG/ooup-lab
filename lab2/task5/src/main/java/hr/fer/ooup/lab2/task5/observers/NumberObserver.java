package hr.fer.ooup.lab2.task5.observers;

public interface NumberObserver {

    void update();
}
