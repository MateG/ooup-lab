package hr.fer.ooup.lab2.task5;

import hr.fer.ooup.lab2.task5.observers.AverageObserver;
import hr.fer.ooup.lab2.task5.observers.MeanObserver;
import hr.fer.ooup.lab2.task5.observers.SumObserver;
import hr.fer.ooup.lab2.task5.sources.KeyboardSource;

public class NumberSeriesTest {

    public static void main(String[] args) {
        NumberSeries series = new NumberSeries(new KeyboardSource());
        new SumObserver(series);
        new AverageObserver(series);
        new MeanObserver(series);
        series.start();
    }
}
