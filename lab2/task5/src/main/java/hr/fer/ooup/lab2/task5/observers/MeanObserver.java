package hr.fer.ooup.lab2.task5.observers;

import hr.fer.ooup.lab2.task5.NumberSeries;

import java.util.Arrays;
import java.util.List;

public class MeanObserver extends AbstractNumberObserver {

    public MeanObserver(NumberSeries series) {
        super(series);
    }

    @Override
    public void update() {
        List<Integer> values = series.getValues();
        int size = values.size();
        int[] copy = new int[size];
        for (int i = 0; i < size; i ++) {
            copy[i] = values.get(i);
        }
        Arrays.sort(copy);
        System.out.println(copy[size/2]);
    }
}
