package hr.fer.ooup.lab2.task5.observers;

import hr.fer.ooup.lab2.task5.NumberSeries;

public class SumObserver extends AbstractNumberObserver {

    public SumObserver(NumberSeries series) {
        super(series);
    }

    @Override
    public void update() {
        int sum = 0;
        for (int value : series.getValues()) {
            sum += value;
        }
        System.out.println(sum);
    }
}
