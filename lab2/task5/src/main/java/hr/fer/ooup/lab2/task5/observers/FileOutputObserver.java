package hr.fer.ooup.lab2.task5.observers;

import hr.fer.ooup.lab2.task5.NumberSeries;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Date;

public class FileOutputObserver extends AbstractNumberObserver {

    private Path filepath;

    public FileOutputObserver(NumberSeries series, Path filepath) {
        super(series);
        this.filepath = filepath;
    }

    @Override
    public void update() {
        try (BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(Files.newOutputStream(
                        filepath, StandardOpenOption.APPEND)))) {
            StringBuilder sb = new StringBuilder();
            sb.append(new Date()).append('\n');
            series.getValues().forEach(value -> {
                sb.append(value).append(' ');
            });
            sb.append('\n');
            writer.write(sb.toString());
        } catch (IOException e) {
            // Maybe log the error somewhere
            // Ignored for simplicity
        }
    }
}
