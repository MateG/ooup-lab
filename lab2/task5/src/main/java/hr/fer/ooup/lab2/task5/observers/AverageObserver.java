package hr.fer.ooup.lab2.task5.observers;

import hr.fer.ooup.lab2.task5.NumberSeries;

import java.util.List;

public class AverageObserver extends AbstractNumberObserver {

    public AverageObserver(NumberSeries series) {
        super(series);
    }

    @Override
    public void update() {
        List<Integer> values = series.getValues();
        int sum = 0;
        for (int value : values) {
            sum += value;
        }
        System.out.println((double) sum / values.size());
    }
}
