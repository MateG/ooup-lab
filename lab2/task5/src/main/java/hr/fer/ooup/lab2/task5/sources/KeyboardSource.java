package hr.fer.ooup.lab2.task5.sources;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class KeyboardSource extends AbstractReaderSource {

    public KeyboardSource() {
        reader = new BufferedReader(new InputStreamReader(System.in));
    }
}
