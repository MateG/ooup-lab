#include <stdio.h>
#include <string.h>

#define SIZE(x) (sizeof(x) / sizeof((x)[0]))

const void* mymax(const void* base, size_t nmemb, size_t size,
        int (*compar)(const void*, const void*)) {
    int i;
    void* current = (void*) base;
    void* max = current;
    for (i = 1; i < nmemb; i ++) {
        current += size;
        if (compar(current, max)) max = current;
    }
    return max;
}

int gt_int(const void* first, const void* second) {
    return *(int*) first > *(int*) second ? 1 : 0;
}

int gt_char(const void* first, const void* second) {
    return *(char*) first > *(char*) second ? 1 : 0;
}

int gt_str(const void* first, const void* second) {
    return strcmp(*(const char**) first, *(const char**) second) > 0 ?
        1 : 0;
}

int main() {
    int arr_int[] = {1, 3, 5, 7, 4, 6, 9, 2, 0};
    char arr_char[] = "Suncana strana ulice";
    const char* arr_str[] = {
        "Gle", "malu", "vocku", "poslije", "kise",
        "Puna", "je", "kapi", "pa", "ih", "njise"
    };

    printf("Max int: %d\n",
        *(int*) mymax(arr_int, SIZE(arr_int), sizeof(int), gt_int));
    printf("Max char: %c\n",
        *(char*) mymax(arr_char, SIZE(arr_char), sizeof(char), gt_char));
    printf("Max str: %s\n",
        *(char**) mymax(arr_str, SIZE(arr_str), sizeof(char*), gt_str));
}
