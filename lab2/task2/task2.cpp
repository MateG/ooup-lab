#include <iostream>
#include <cstring>
#include <vector>
#include <set>

#define END(x) (&x[sizeof(x)/sizeof(*x)])

template<typename Iterator, typename Predicate>
Iterator mymax(Iterator cur, Iterator last, Predicate pred) {
    Iterator max = cur;
    while (cur != last) {
        if (pred(*cur, *max)) max = cur;
        cur ++;
    }
    return max;
}

int gt_int(int first, int second) {
     return first > second ? 1 : 0;
}

int gt_char(char first, char second) {
     return first > second ? 1 : 0;
}

int gt_str(const char* first, const char* second) {
     return std::strcmp(first, second) > 0 ? 1 : 0;
}

int arr_int[] = {1, 3, 5, 7, 4, 6, 9, 2, 0};
char arr_char[] = "Suncana strana ulice";
const char* arr_str[] = {
    "Gle", "malu", "vocku", "poslije", "kise",
    "Puna", "je", "kapi", "pa", "ih", "njise"
};

std::vector<int> vector_int = {1, 3, 5, 7, 4, 6, 9, 2, 0};
std::vector<int> vector_char = {'S', 'u', 'n', 'c', 'a', 'n', 'a', ' ',
    's', 't', 'r', 'a', 'n', 'a', ' ', 'u', 'l', 'i', 'c', 'e'};
std::vector<const char*> vector_str = {
    "Gle", "malu", "vocku", "poslije", "kise",
    "Puna", "je", "kapi", "pa", "ih", "njise"
};

std::set<int> set_int = {1, 3, 5, 7, 4, 6, 9, 2, 0};
std::set<char> set_char = {'S', 'u', 'n', 'c', 'a', 'n', 'a', ' ',
    's', 't', 'r', 'a', 'n', 'a', ' ', 'u', 'l', 'i', 'c', 'e'};
std::set<const char*> set_str = {
    "Gle", "malu", "vocku", "poslije", "kise",
    "Puna", "je", "kapi", "pa", "ih", "njise"
};

int main() {
    std::cout << "arr_int: "
        << *mymax(&arr_int[0], END(arr_int), gt_int) << "\n";
    std::cout << "arr_char: "
        << *mymax(&arr_char[0], END(arr_char), gt_char) << "\n";
    std::cout << "arr_str: "
        << *mymax(&arr_str[0], END(arr_str), gt_str) << "\n";

    std::cout << "vector_int: "
        << *mymax(vector_int.begin(), vector_int.end(), gt_int) << "\n";
    std::cout << "vector_char: "
        << (char) *mymax(vector_char.begin(), vector_char.end(), gt_char) << "\n";
    std::cout << "vector_str: "
        << *mymax(vector_str.begin(), vector_str.end(), gt_str) << "\n";

    std::cout << "set_int: "
        << *mymax(set_int.begin(), set_int.end(), gt_int) << "\n";
    std::cout << "set_char: "
        << *mymax(set_char.begin(), set_char.end(), gt_char) << "\n";
    std::cout << "set_str: "
        << *mymax(set_str.begin(), set_str.end(), gt_str) << "\n";
}
