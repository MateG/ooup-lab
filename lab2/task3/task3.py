def mymax(iterable, key = None):
    if not key:
        key = lambda x: iterable.index(x)
    max_x = max_key = None
    for x in iterable:
        current_key = key(x)
        if not max_key or current_key > max_key:
            max_x = x
            max_key = current_key
    return max_x

maxint = mymax([1, 3, 5, 7, 4, 6, 9, 2, 0])
maxchar = mymax("Suncana strana ulice")
maxstring = mymax([
    "Gle", "malu", "vocku", "poslije", "kise",
    "Puna", "je", "kapi", "pa", "ih", "njise"])

print("maxint", maxint)
print("maxchar", maxchar)
print("maxstring", maxstring)

D = {'burek':8, 'buhtla':5}
maxproizvod = mymax(D, D.get)

print("maxproizvod", maxproizvod)

names = [("Iva", "Ivic"), ("Pero", "Peric"),
    ("Ana", "Anic"), ("Marko", "Markic")]
maxname = mymax(names, lambda x: x[0] + x[1])
print("maxname", maxname)
